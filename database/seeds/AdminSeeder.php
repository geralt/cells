<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'phone' => '77777777777',
            'password' => \Illuminate\Support\Facades\Hash::make('3@dmin'),
            'role' => 'admin'
        ]);
    }
}
