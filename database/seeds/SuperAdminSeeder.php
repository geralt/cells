<?php

use Illuminate\Database\Seeder;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'phone' => '79999999999',
            'password' => \Illuminate\Support\Facades\Hash::make('7@dmin'),
            'role' => 'superadmin'
        ]);
    }
}
