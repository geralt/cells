<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cells', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('number');
            $table->integer('device_id');
            $table->tinyInteger('current_temp')->nullable();
            $table->tinyInteger('is_door_open')->default(0);
            $table->integer('status')->default(0);
            $table->integer('rent_id')->nullable();
            $table->integer('opens_by_rent')->default(0);
            $table->integer('opens_total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cells');
    }
}
