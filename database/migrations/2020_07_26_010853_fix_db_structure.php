<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixDbStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cells', function (Blueprint $table) {
            $table->dropColumn('opens_by_rent');
        });
        Schema::table('rents', function (Blueprint $table) {
            $table->integer('opens_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cells', function (Blueprint $table) {
            $table->tinyInteger('opens_by_rent')->default(0);
        });
        Schema::table('rents', function (Blueprint $table) {
            $table->dropColumn('opens_count');
        });
    }
}
