'use strict'

const mix = require('laravel-mix')

const domain = 'admin';

mix
    .sass('resources/assets/sass/app.scss', 'public/css')
    .combine([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',], 'public/js/app.js')


    const dir = domain.replace(/\./g, '_')

    mix.js(`resources/assets/js/admin/app.js`, `public/${dir}/js`)
    mix.js(`resources/assets/js/admin/devices.js`, `public/${dir}/js`)

