window.$ = window.jQuery = require("jquery")
window.popper = require('popper.js');
window.bootstrap = require("bootstrap")
window.swal = require('sweetalert')
window.select2 = require('select2')
window.datetimepicker = require('jquery-datetimepicker')
window.selectpicker = require('bootstrap-select')

$('[data-toggle="tooltip"]').tooltip();
window.ruLocalization = {
        months:[
            'Январь','Февраль','Март','Апрель',
            'Maй','Июнь','Июль','Август',
            'Сентябрь','Октябрь','Ноябрь','Декабрь',
        ],
        dayOfWeek:[
            "Вс", "Пн", "Вт", "Ср",
            "Чт", "Пт", "Сб",
        ]
}

let showModal = (body, callback, raw = false) => {
    swal({
        title: 'Вы уверены?',
        text: "Вы не сможете отменить это действие!",
        icon: 'warning',
        buttons: {
            confirm: {
                text: raw && raw.buttonText ? raw.buttonText : "Да, удалить!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            },
            cancel: {
                text: "Отменить",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            }
        },
        dangerMode: true,
    }).then((result) => {
        if (result) {
            callback()
        }
    })
}

$('[data-form-submit]').on('click', function (e) {
    e.preventDefault()

    let $el = $(this)
    let options = $el.data('form-submit')

    if (options.confirm === undefined) {
        $(options.target).submit()
    } else {
        showModal(
            options.confirm,
            () => $(options.target).submit(),
            options
        )
    }
})

$('.selectpicker').selectpicker({
    noneSelectedText: "Ничего не выбрано",
    style: "",
    styleBase: "form-control"
})
