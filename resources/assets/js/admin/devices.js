'use strict'

let c3 = require('c3')
let id;
let period = '1d'

$('.chart').on('click', function () {
    id = $(this).data('id')
    $('#modal-stats').modal('show');
    getData(id, period)
})

$('.range').on('click', function() {
    $('.range').each((i, item) => {
        if($(item).hasClass('btn-info')) {
            $(item).removeClass('btn-info').addClass('btn-outline-info');
        }
    });

    $(this).removeClass('btn-outline-info').addClass('btn-info')
    period = $(this).data('stats-range')
    getData(id, period)
});

function getData(id, period) {
    $.get(`/devices/${id}/temp/${period}`).done(function (data) {
        let chart = prepareData(data)
        c3.render(chart)
    });
}

function prepareData(data) {
    let chart = [];
    chart.push(["Температура", "Дата"])
    data.temps.forEach((item) => {
        chart.push([item.temp, item.created_at]);
    });

    return chart;
}

const chart = {}

chart.options = {
    bindto: '#chart-stats',
    data: {
        x: 'Дата',
        xFormat: '%Y-%m-%d %H:%M',
        rows: [],
        type: 'area-spline'
    },
    axis: {
        x: {
            type: 'timeseries',
        },
    },
    grid: {
        x: { show: true },
        y: { show: true }
    }
}

c3.render = function (rows) {
    if (chart.object) {
        let xFormat = period === '1w' || period === '30d' ? '%Y-%m-%d' : '%Y-%m-%d %H:%M';

        chart.object.load({ rows, unload: true, type: 'area-spline', xFormat: xFormat })
    } else {
        const options = { data: { rows } }
        chart.object = c3.generate($.extend(true, chart.options, options))
    }
}


$('document').ready(function () {
    jQuery.datetimepicker.setLocale('ru');
    $('#start_date').datetimepicker({
        i18n:{
            ru: window.ruLocalization
        },
        timepicker:true,
        showSecond: false,
        formatDate:'Y-m-d',
        formatTime: 'H:i',
        format: 'Y-m-d H:i',
    });

    $('#end_date').datetimepicker({
        i18n:{
            ru: window.ruLocalization
        },
        timepicker:true,
        showSecond: false,
        formatDate:'Y-m-d',
        formatTime: 'H:i',
        format: 'Y-m-d H:i',
    });

    $('.download-temp').on('click', function () {
        let url = `/devices/${$(this).data('device-id')}/temp/download`
        $('#download-temp').attr('action', url)
        $('#modal-date-selector').modal('show');
    })

    $('#fire-download').on('click', function () {
        $('#download-temp').submit()
    })
});
