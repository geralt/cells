<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Поле :attribute должен быть принят.',
    'active_url' => 'Поле :attribute не валидный URL.',
    'after' => 'Поле :attribute должен быть датой позже :date.',
    'after_or_equal' => 'Поле :attribute должен быть позже или равен :date.',
    'alpha' => 'Поле :attribute может содержать только буквы.',
    'alpha_dash' => 'Поле :attribute может содержать только буквы, числа, тире и подчеркивания.',
    'alpha_num' => 'Поле :attribute может сожержать только буквы и числа.',
    'array' => 'Поле :attribute должен быть массивом.',
    'before' => 'Поле :attribute должно быть раньше, чем :date.',
    'before_or_equal' => 'Поле :attribute должно быть раньше или равно :date.',
    'between' => [
        'numeric' => 'Поле :attribute должно быть между :min и :max.',
        'file' => 'Поле :attribute должно быть между :min и :max килобайт.',
        'string' => 'Поле :attribute должно быть между :min и :max символов.',
        'array' => 'Поле :attribute должно содержать между :min и :max элементов.',
    ],
    'boolean' => 'Поле :attribute должно быть истинно или ложно.',
    'confirmed' => 'Поле :attribute подтверждение не совпало.',
    'date' => 'Поле :attribute не валидная дата.',
    'date_equals' => 'Поле :attribute должна быть дата, равная :date.',
    'date_format' => 'Поле :attribute не соответствует формату :format.',
    'different' => 'Поле :attribute и :other должны быть различными.',
    'digits' => 'Поле :attribute должно быть :digits цифр.',
    'digits_between' => 'Поле :attribute должно быть от :min до :max цифр.',
    'dimensions' => 'Поле :attribute имеет некорректное расширение.',
    'distinct' => 'Поле :attribute имеет дублируещееся значение.',
    'email' => 'Поле :attribute должно быть валидным email адресом.',
    'ends_with' => 'Поле :attribute должно заканчиваться на одно из значений: :values.',
    'exists' => 'Выбранный аттрибут :attribute неверный.',
    'file' => 'Поле :attribute должно быть файлом.',
    'filled' => 'Поле :attribute должно содержать значение.',
    'gt' => [
        'numeric' => 'Поле :attribute должно быть больше, чем :value.',
        'file' => 'Поле :attribute должно больше, чем :value килобайт.',
        'string' => 'Поле :attribute должно быть больше, чем :value символов.',
        'array' => 'Поле :attribute должно быть больше, чем :value элементов.',
    ],
    'gte' => [
        'numeric' => 'Поле :attribute должно быть больше или равно :value.',
        'file' => 'Поле :attribute должно быть больше или равно :value килобайт.',
        'string' => 'Поле :attribute должно быть больше или равно :value символов.',
        'array' => 'Поле :attribute должно содержать :value элементов или больше.',
    ],
    'image' => 'Поле :attribute должно быть изображением.',
    'in' => 'Выбранный элемент :attribute неверный.',
    'in_array' => 'Поле :attribute не находится в массиве :other.',
    'integer' => 'Поле :attribute должно быть числом.',
    'ip' => 'Поле :attribute должно быть валидным IP адресом.',
    'ipv4' => 'Поле :attribute должно быть валидным IPv4 адресом.',
    'ipv6' => 'Поле :attribute должно быть валидным IPv6 адресом.',
    'json' => 'Поле :attribute должно быть валидной JSON строкой.',
    'lt' => [
        'numeric' => 'Поле :attribute должно быть меньше, чем :value.',
        'file' => 'Поле :attribute должно быьт меньше :value килобайт.',
        'string' => 'Поле :attribute должно содержать меньше :value символов.',
        'array' => 'Поле :attribute должно сожержать меньше :value элементов.',
    ],
    'lte' => [
        'numeric' => 'Поле :attribute должно быть меньше или равно :value.',
        'file' => 'Поле :attribute должно быть меньше или равно :value килобайт.',
        'string' => 'Поле :attribute должно содержать меньше или равно :value символов.',
        'array' => 'Поле :attribute должно сожержать меньше или :value элементов.',
    ],
    'max' => [
        'numeric' => 'Поле :attribute не может быть больше :max.',
        'file' => 'Поле :attribute не может быть больше, чем :max килобайт.',
        'string' => 'Поле :attribute не может быть больше, чем :max символов.',
        'array' => 'Поле :attribute не может содержать больше, чем :max элементов.',
    ],
    'mimes' => 'Поле :attribute должно быть файлом типа: :values.',
    'mimetypes' => 'Поле :attribute должно быть файлом типа: :values.',
    'min' => [
        'numeric' => 'Поле :attribute должно быть как минимум равно :min.',
        'file' => 'Поле :attribute должно быть как минимум :min килобайт.',
        'string' => 'Поле :attribute должно сожержать как минимум :min символов.',
        'array' => 'Поле :attribute должно содержать как минимум :min элементов.',
    ],
    'not_in' => 'Выбранное значение поля :attribute неверно.',
    'not_regex' => 'Формат поля :attribute неверный.',
    'numeric' => 'Поле :attribute должно быть числом.',
    'password' => 'Неверный пароль.',
    'present' => 'Поле :attribute должно быть заполнено.',
    'regex' => 'Формат поля :attribute неверный.',
    'required' => 'Поле :attribute должно быть заполнено.',
    'required_if' => 'Поле :attribute должно быть заполнено, когда :other равно :value.',
    'required_unless' => 'Поле :attribute должно быть заполнено до тех пор, пока :other сожержит значения :values.',
    'required_with' => 'Поле :attribute должно быть заполнено, пока есть :values.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
