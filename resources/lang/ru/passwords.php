<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Мы отослали ссылку на смену пароля вам на email!',
    'throttled' => 'Пожалуйста, подождите перед следующей попыткой.',
    'token' => 'Неверный токен смены пароля.',
    'user' => "Мы не можем найти пользователя с таким email адресом.",

];
