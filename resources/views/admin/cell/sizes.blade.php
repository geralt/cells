@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    Укажите размеры ячеек
                </span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">

                        @include('admin.partials.errors')

                        <form action="{{ route('admin::cells.store.size', $device) }}" method="post">
                            {{ csrf_field() }}

                            @foreach($cells as $cell)
                                @if(($loop->index + 1) % 2 === 1)
                                    <div class="row form-group">
                                @endif
                                    <div class="col-md-6">
                                        <label for="user_id">Размеры ячейки №{{ $loop->index + 1 }} ( ШхВхГ ):</label>
                                        <select class="form-control" name="size[{{$cell->id}}]">
                                            <option>Выберите размер</option>
                                            @foreach($possibleSizes as $size)
                                                <option value="{{ $size->id }}" @if($cell->size_id == $size->id) selected @endif>{{ implode(' x ', [$size->width, $size->height, $size->depth]) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @if(($loop->index + 1) % 2 === 0)
                                    </div>
                                @endif
                            @endforeach

                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">
                                    Сохранить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
