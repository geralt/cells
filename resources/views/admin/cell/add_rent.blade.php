@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-server" aria-hidden="true"></i>
                    <a href="{{ url('cells/' . $cell->id . '/show') }}"> Вернуться к ячейке №{{ $cell->number }}</a>
                </span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">

                        @include('admin.partials.errors')

                        <form action="{{ route('admin::cell.add.rent', $cell) }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="user_id">Пользователь</label>
                                <select id="select2" class="form-control form-control_select" name="user_id"></select>
                            </div>

                            <div class="form-group">
                                <label for="start_date">Начало аренды</label>
                                <input type="text" name="start_date" class="form-control input-lg"
                                       id="start_date"
                                       value="{{ \Carbon\Carbon::now()->format('Y-m-d H:i') }}">
                            </div>
                            <div class="form-group">
                                <label for="start_date">Конец аренды</label>
                                <input type="text" name="end_date" class="form-control input-lg"
                                      id="end_date"
                                      value="{{ \Carbon\Carbon::now()->addHours(48)->format('Y-m-d H:i') }}">
                            </div>


                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">
                                    Сохранить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('document').ready(function () {
            jQuery.datetimepicker.setLocale('ru');
            $('#start_date').datetimepicker({
                i18n:{
                    ru: window.ruLocalization
                },
                timepicker:true,
                showSecond: false,
                formatDate:'Y-m-d',
                formatTime: 'H:i',
                format: 'Y-m-d H:i',
                minDate: '{{ \Carbon\Carbon::now()->format('d.m.Y') }}'
            });

            $('#end_date').datetimepicker({
                i18n:{
                    ru: window.ruLocalization
                },
                timepicker:true,
                showSecond: false,
                formatDate:'Y-m-d',
                formatTime: 'H:i',
                format: 'Y-m-d H:i',
                minDate: '{{ \Carbon\Carbon::now()->format('d.m.Y') }}'
            });

            $('#select2').select2({
                theme: "bootstrap",
                ajax: {
                    url: '{{ route('admin::users.available') }}',
                    dataType: 'json',
                    type: 'POST',
                    data: function (params) {
                        return {
                            _token: '{{ csrf_token() }}',
                            q: params.term,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.first_name + ' ' + item.last_name,
                                    id: item.id,
                                    data: item
                                };
                            })
                        };
                    },
                    cache: true
                },
            });
        });
    </script>
@endpush
