@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-server" aria-hidden="true"></i>
                    <span style="color: grey"> Ячейка №{{ $cell->number }}</span>
                </span>
                @if($cell->status == \App\Models\Cell::FREE)
                    <a href="{{ route('admin::cell.show.add.rent', $cell) }}" class="pull-right"><i class="fa fa-plus"></i> Добавить аренду</a>
                @endif
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('admin::cell.update', $cell) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="form-group">
                                <label for="mac">Устройство</label>
                                <input type="text" name="mac" class="form-control input-lg" id="mac"
                                       value="{{ $cell->device->mac }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="address">Адрес</label>
                                <input type="text" name="address" class="form-control input-lg" id="address"
                                       value="{{ $cell->device->place->address }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="doorStatus">Статус двери</label>
                                <input type="text" name="door_status" class="form-control input-lg" id="door_status"
                                       value="{{ $doorStatus }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="cell_status">Статус ячейки</label>
                                <select class="form-control" name="status">
                                    <option value="{{ \App\Models\Cell::FREE }}" @if($cell->status == \App\Models\Cell::FREE) selected @endif>Свободна</option>
                                    <option value="{{ \App\Models\Cell::IN_USE }}" disabled @if($cell->status == \App\Models\Cell::IN_USE) selected @endif>Используется</option>
                                    <option value="{{ \App\Models\Cell::ON_SERVICE }}" @if($cell->status == \App\Models\Cell::ON_SERVICE) selected @endif>На обслуживании</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="user_id">Размеры ячейки ( ШхВхГ ):</label>
                                <select class="form-control" name="size_id">
                                    <option>Выберите размер</option>
                                    @foreach($sizes as $size)
                                        <option value="{{ $size->id }}" @if($cell->size_id == $size->id) selected @endif>{{ implode(' x ', [$size->width, $size->height, $size->depth]) }}</option>
                                    @endforeach
                                </select>
                            </div>

                            @if($cell->status == \App\Models\Cell::IN_USE)
                                <?php $rent = $cell->current_rent; ?>
                                @if($rent)
                                    <div class="form-group">
                                        @if($rent->user)
                                            <label for="doorStatus">
                                                В аренде у: <a href="{{ route('admin::users.edit', $rent->user_id) }}">{{ $rent->user->fullName }}</a>
                                                <button type="button" class="btn btn-danger btn-sm" id="cancel-rent">Отменить аренду</button>
                                            </label>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="start_date">Начало аренды</label>
                                        <input type="text" name="start_date" class="form-control input-lg"
                                               id="start_date" readonly
                                               value="{{ $rent->start_date->format('Y-m-d H:i') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="start_date">Конец аренды</label>
                                        <input type="text" name="end_date" class="form-control input-lg"
                                               id="end_date"
                                               value="{{ $rent->end_date->format('Y-m-d H:i') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="opens_total">Открытий замка (за аренду)</label>
                                        <input type="text" name="opens_total" class="form-control input-lg" id="opens_total"
                                               value="{{ $rent->opens_count }}" readonly>
                                    </div>
                                @endif
                            @endif

                            <div class="form-group">
                                <label for="opens_total">Открытий замка (всего)</label>
                                <input type="text" name="opens_total" class="form-control input-lg" id="opens_total"
                                       value="{{ $cell->opens_total }}" readonly>
                            </div>
                            <div class="form-group">
                               <button class="btn btn-primary" type="submit">
                                   Сохранить
                               </button>
                                <button class="btn btn-danger" id="open" type="button">
                                   Открыть дверь
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('document').ready(function () {
            jQuery.datetimepicker.setLocale('ru');
            $('#start_date').datetimepicker({
                i18n:{
                    ru: window.ruLocalization
                },
                timepicker:true,
                showSecond: false,
                formatDate:'Y-m-d',
                formatTime: 'H:i',
                format: 'Y-m-d H:i',
                minDate: '{{ \Carbon\Carbon::now()->format('d.m.Y') }}'
            });

            $('#end_date').datetimepicker({
                i18n:{
                    ru: window.ruLocalization
                },
                timepicker:true,
                showSecond: false,
                formatDate:'Y-m-d',
                formatTime: 'H:i',
                format: 'Y-m-d H:i',
                minDate: '{{ \Carbon\Carbon::now()->format('d.m.Y') }}'
            });

            $('#open').on('click', function() {
                swal({
                    title: "Вы уверены?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            window.location = '{{ route('admin::cell.open', $cell) }}'
                        }
                    })
            });

            $('#cancel-rent').on('click', function() {
                swal({
                    title: "Вы уверены?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            window.location = '{{ route('admin::cell.cancel.rent', $cell) }}'
                        }
                    })
            });
        });
    </script>
@endpush
