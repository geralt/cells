@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    Места
                </span>
                <a href="{{ route('admin::places.create') }}" class="pull-right"><i class="fa fa-plus"></i> Новое место</a>
            </div>
            <div class="card-body">
                @include('admin.places.index.filter')
                <br>

                <div class="row">
                    <div class="col-md-12">
                        @if($places->count() > 0)
                            <table class="table">
                                @include('admin.places.index.head')
                                <tbody>
                                @foreach($places as $item)
                                    @include('admin.places.index.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                            {{ $places->links() }}
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
