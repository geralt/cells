<tr>
    <td>{{ $item->id }}</td>
    <td>{{ $item->title }}</td>
    <td>{{ $item->address }}</td>
    <td>{{ $item->company }}</td>
    <td>{{ $item->responsible_person }}</td>
    <td>{{ $item->phone }}</td>
    <td>{{ $item->comment }}</td>
    <td>
        <form id="delete-place-{{ $item->id }}" method="post" action="{{ route("admin::places.destroy", $item) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

        <a href="{{ route("admin::places.edit", $item) }}" class="btn btn-xs btn-warning">
            <span class="fa fa-pencil"></span>
        </a>
        <button class="btn btn-xs btn-danger" data-id="delete-place-{{ $item->id }}" data-form-submit="{{ json_encode(['target' => "#delete-place-{$item->id}", 'confirm' => true]) }}">
            <span class="fa fa-trash"></span>
        </button>
    </td>
</tr>
