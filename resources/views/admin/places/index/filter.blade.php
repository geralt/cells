<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin::places') }}" method="get">
                <div class="input-group">
                    <label for="address" class="filter-label">Адрес</label>
                    <input type="text" id="address" class="form-control form-control-sm filter-el" name="address"
                           value="{{ old('address', request()->address) }}">

                    <label for="company" class="filter-label">Компания</label>
                    <input type="text" id="company" class="form-control form-control-sm filter-el" name="company"
                           value="{{ old('company', request()->company) }}">

                    <button class="btn btn-outline-secondary btn-sm filter-button" ><span class="fa fa-filter"></span> Фильтровать</button>
                    <a href="{{ route('admin::places') }}" class="btn btn-outline-secondary btn-sm filter-button" style="padding-top: 7px;"><span class="fa fa-times"></span> Отмена</a>
                </div><!-- /input-group -->
            </form>
        </div>
    </div>
</div>
