@extends('layouts.admin')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><a href="{{ route('admin::places') }}"><i
                                    class="fa fa-map-marker"></i> Места</a> / Редактировать</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">

                                @include('admin.partials.errors')

                                <form action="{{ route('admin::places.update', $place) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field("PUT") }}

                                    <div class="form-group">
                                        <label for="title">Название</label>
                                        <input type="text" name="title" class="form-control input-lg" id="title"
                                               placeholder="Title" value="{{ old('title', $place->title) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Адрес</label>
                                        <input type="text" name="address" class="form-control input-lg" id="address"
                                               placeholder="Пл. Карла Маркса 1" value="{{ old('address', $place->address) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="company">Компания</label>
                                        <input type="text" name="company" class="form-control input-lg" id="company"
                                               placeholder="Почта России" value="{{ old('company', $place->company) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="person">Ответсвенное лицо</label>
                                        <input type="text" name="responsible_person" class="form-control input-lg" id="person"
                                               placeholder="Иваг Иванович Иванов" value="{{ old('responsible_person', $place->responsible_person) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Телефон</label>
                                        <input type="text" name="phone" class="form-control input-lg" id="phone"
                                               placeholder="79287771234" value="{{ old('phone', $place->phone) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="company">Широта</label>
                                        <input type="text" class="form-control" value="{{ old('latitude', $place->latitude) }}" name="latitude" placeholder="57.1456">
                                    </div>

                                    <div class="form-group">
                                        <label for="company">Долгота</label>
                                        <input type="text" class="form-control" value="{{ old('longitude', $place->longitude) }}" name="longitude" placeholder="34.3658">
                                    </div>

                                    <div class="form-group">
                                        <label for="comment">Комментарий</label>
                                        <textarea class="form-control input-lg" id="comment" name="comment">{{ old('comment', $place->comment) }}</textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </form>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
