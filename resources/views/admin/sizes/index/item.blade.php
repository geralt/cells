<tr>
    <td>{{ $item->alias ?? '-' }}</td>
    <td>{{ $item->width }}</td>
    <td>{{ $item->height }}</td>
    <td>{{ $item->depth }}</td>
    <td>
        <form id="delete-size-{{ $item->id }}" method="post" action="{{ route("admin::cells.sizes.destroy", $item->id) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

        <a href="{{ route("admin::cells.sizes.edit", $item) }}" class="btn btn-xs btn-warning">
            <span class="fa fa-pencil"></span>
        </a>
        <button class="btn btn-xs btn-danger" data-id="delete-size-{{ $item->id }}" data-form-submit="{{ json_encode(['target' => "#delete-size-{$item->id}", 'confirm' => true]) }}">
            <span class="fa fa-trash"></span>
        </button>
    </td>
</tr>
