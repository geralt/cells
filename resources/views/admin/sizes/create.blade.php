@extends('layouts.admin')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="{{ route('admin::cells.sizes') }}">
                                <i class="fa fa-arrows-v"></i> Размеры ячеек</a> / Создать
                        </h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">

                                @include('admin.partials.errors')

                                <form action="{{ route('admin::cells.sizes.store') }}" method="post">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="width">Алиас</label>
                                        <input type="text" name="alias" class="form-control input-lg" id="width"
                                               value="{{ old('alias') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="width">Ширина (см)</label>
                                        <input type="text" name="width" class="form-control input-lg" id="width"
                                               value="{{ old('width') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="height">Высота (см)</label>
                                        <input class="form-control" id="height" type="number" name="height"
                                               value="{{ old('height') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="depth">Глубина (см)</label>
                                        <input type="text" class="form-control" value="{{ old('depth') }}" name="depth">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
