@extends('layouts.admin')



@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <a href="{{ route('admin::devices') }}"><i
                        class="fa fa-server"></i> Устройства</a>
                /
                <span class="card-title">
                    <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    Размеры ячеек
                </span>
                <a href="{{ route('admin::cells.sizes.create') }}" class="pull-right"><i class="fa fa-plus"></i> Новый размер</a>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        @if($sizes->count() > 0)
                            <table class="table">
                                @include('admin.sizes.index.head')
                                <tbody>
                                @foreach($sizes as $item)
                                    @include('admin.sizes.index.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                            {{ $sizes->links() }}
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
