@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-server" aria-hidden="true"></i>
                    Устройства / <span style="color: grey"> {{ $device->mac }}, {{ $device->place->address }}</span>
                </span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        @if($cells->count() > 0)
                            <table class="table">
                                @include('admin.devices.cells.head')
                                <tbody>
                                @foreach($cells as $item)
                                    @include('admin.devices.cells.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
