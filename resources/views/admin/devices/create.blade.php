@extends('layouts.admin')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><a href="{{ route('admin::devices') }}"><i
                                    class="fa fa-server"></i> Устройства</a> / Создать
                            <a href="{{ route('admin::cells.sizes') }}" class="pull-right"><i class="fa fa-plus"></i> Размеры</a>
                        </h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">

                                @include('admin.partials.errors')

                                <form action="{{ route('admin::devices.store') }}" method="post">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="mac">Заводской номер</label>
                                        <input type="text" name="mac" class="form-control input-lg" id="mac"
                                               placeholder="01:01:01:01:01:01:01:01" value="{{ old('mac') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="person">Адрес</label>
                                        <select class="form-control" name="place_id">
                                            @foreach($places as $place)
                                                <option value="{{ $place->id }}">{{ $place->address }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="company">Требуемая температура</label>
                                        <select class="form-control" name="target_temp">
                                            @foreach($possibleTemps as $temp)
                                                <option value="{{ $temp }}">{{ $temp }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="cells_count">Количество ячеек</label>
                                        <input class="form-control" id="cells_count" type="number" name="cells_count">
                                    </div>

                                    <div class="form-group">
                                        <label for="company">Ip адрес (включая порт)</label>
                                        <input type="text" class="form-control" value="{{ old('ip_address') }}" name="ip_address" placeholder="http://127.0.0.1:8080">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
