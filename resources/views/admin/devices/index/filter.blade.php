<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin::devices') }}" method="get">
                <div class="input-group">
                    <label for="address" class="filter-label">Адрес</label>
                    <input type="text" id="address" class="form-control form-control-sm filter-el" name="address"
                           value="{{ old('address', request()->address) }}">

                    <label for="mac" class="filter-label">Заводской номер</label>
                    <input type="text" id="mac" class="form-control form-control-sm filter-el" name="mac"
                           value="{{ old('mac', request()->mac) }}">

                    <button class="btn btn-outline-secondary btn-sm filter-button" ><span class="fa fa-filter"></span> Фильтровать</button>
                    <a href="{{ route('admin::devices') }}" class="btn btn-outline-secondary btn-sm filter-button" style="padding-top: 7px;"><span class="fa fa-times"></span> Отмена</a>
                </div><!-- /input-group -->
            </form>
        </div>
    </div>
</div>
