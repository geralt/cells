<div id="modal-date-selector" class="modal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><small class="fa fa-bar-chart"></small> <span class="name">Выберите период:</span></h4>
                <button type="button" data-dismiss="modal" class="close"><span>&times;</span></button>
            </div>
            <div class="modal-body">
                <form method="post" id="download-temp" action="">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <label for="start_date">С&nbsp;</label>
                            <input type="text" name="start_date" class="form-control input-lg"
                                   id="start_date" value="{{ now()->subDays(2)->format('Y-m-d H:i') }}" style="max-width: 500px;">
                        </div>
                        <div class="col-md-6">
                            <label for="end_date">По &nbsp;</label>
                            <input type="text" name="end_date" class="form-control input-lg"
                                   id="end_date" value="{{ now()->format('Y-m-d H:i') }}" style="max-width: 500px;">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="fire-download" class="btn btn-success">
                    <span class="fa fa-download"></span> Скачать
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>
            </div>
        </div>
    </div>
</div>
