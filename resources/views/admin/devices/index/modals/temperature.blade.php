<div id="modal-stats" class="modal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><small class="fa fa-bar-chart"></small> <span class="name"></span></h4>
                <button type="button" data-dismiss="modal" class="close"><span>&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="stats-range" class="mb-md">
                    <button class="btn btn-outline-info btn-sm range" data-stats-range="15min">15 минут</button>
                    <button class="btn btn-outline-info btn-sm range" data-stats-range="1h">1 час</button>
                    <button class="btn btn-outline-info btn-sm range" data-stats-range="4h">4 часа</button>
                    <button class="btn btn-info btn-sm range" data-stats-range="1d">1 день</button>
                    <button class="btn btn-outline-info btn-sm range" data-stats-range="1w">7 дней</button>
                    <button class="btn btn-outline-info btn-sm range" data-stats-range="30d">30 дней</button>
                </div>
                <div id="chart-stats"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>
            </div>
        </div>
    </div>
</div>
