<tr @if(abs($item->current_temp - $item->target_temp) > 5) style="background-color: #fc929c;" @endif >
    <td>{{ $item->id }}</td>
    <td>{{ $item->mac }}</td>
    <td>{{ $item->place->address }}</td>
    <td>
        @if($item->current_temp > 0) +@endif{{ round($item->current_temp) }} / @if($item->target_temp > 0) +@endif{{ $item->target_temp }}
        {{--@if($item->id == 17)
                {{ dump($item->getLastTemp()->created_at->diffInMinutes(\Carbon\Carbon::now(), true)) }}
            @endif--}}
        @if( !$item->getLastTemp() || $item->getLastTemp()->created_at->diffInMinutes() >= 20)
            <i class="fa fa-exclamation" style="color: red" data-toggle="tooltip" data-placement="top" title="Не приходят обновления температуры"></i>
        @endif
    </td>
    <td>
       <span class="fa fa-check" style="color: green;" data-toggle="tooltip"
             data-placement="top"
             title="Свободно"></span> {{ $item->cells->filter(function ($item) { return $item->status === \App\Models\Cell::FREE; })->count() }}
       / <span class="fa fa-clock-o" style="color: orange;" data-toggle="tooltip"
               data-placement="top"
               title="Используется"></span> {{ $item->cells->filter(function ($item) { return $item->status === \App\Models\Cell::IN_USE; })->count() }}
       / <span class="fa fa-times" style="color: red;" data-toggle="tooltip"
               data-placement="top"
               title="На обслуживании"></span> {{ $item->cells->filter(function ($item) { return $item->status === \App\Models\Cell::ON_SERVICE; })->count() }}
       / {{ $item->cells_count }}
    </td>
    <td>
        <form id="delete-device-{{ $item->id }}" method="post" action="{{ route("admin::devices.destroy", $item) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

        <a href="{{ route('admin::cells', $item->id) }}" class="btn btn-info btn-xs">
            <span class="fa fa-th"></span>
        </a>
        <a href="{{ route('admin::cells.choose.size', $item->id) }}" class="btn btn-warning btn-xs">
            <span class="fa fa-arrows-v"></span>
        </a>
        <button type="button" class="btn btn-xs btn-primary chart" data-id="{{ $item->id }}">
            <span class="fa fa-area-chart"></span>
        </button>
        <button type="button" data-device-id="{{ $item->id }}" class="btn btn-xs btn-success download-temp">
            <span class="fa fa-download"></span>
        </button>
        <a href="{{ route("admin::devices.edit", $item) }}" class="btn btn-xs btn-warning">
            <span class="fa fa-pencil"></span>
        </a>
        <button class="btn btn-xs btn-danger" data-id="delete-device-{{ $item->id }}" data-form-submit="{{ json_encode(['target' => "#delete-device-{$item->id}", 'confirm' => true]) }}">
            <span class="fa fa-trash"></span>
        </button>
    </td>
</tr>
