@extends('layouts.admin')



@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-server" aria-hidden="true"></i>
                    Устройства / <span style="color: grey"> Свободно ячеек: {{ $totalFree }} / Использовано ячеек: {{ $totalInUse }} / Ячеек на обслуживании: {{ $totalOnService }}</span>
                </span>
                <a href="{{ route('admin::devices.create') }}" class="pull-right"><i class="fa fa-plus"></i> Новое устройство </a>
                <a href="{{ route('admin::cells.sizes') }}" class="pull-right"><i class="fa fa-arrows-v"></i> Добавить размеры ячеек &nbsp;</a>
            </div>
            <div class="card-body">
                @include('admin.devices.index.filter')
                <br>

                <div class="row">
                    <div class="col-md-12">
                        @if($devices->count() > 0)
                            <table class="table">
                                @include('admin.devices.index.head')
                                <tbody>
                                @foreach($devices as $item)
                                    @include('admin.devices.index.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                            {{ $devices->links() }}
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.devices.index.modals.temperature')
    @include('admin.devices.index.modals.date-selector')
@endsection

@push('scripts')
    <script src="{{ asset('/admin/js/devices.js') }}"></script>
@endpush
