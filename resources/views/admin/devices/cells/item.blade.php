<tr>
    <td> {{ $item->number }} </td>
    <td>
        @if($item->is_door_open)
            <span class="badge badge-danger">Дверь открыта</span>
        @else
            <span class="badge badge-success">Дверь закрыта</span>
        @endif
    </td>
    <td>
        @if($item->status === \App\Models\Cell::IN_USE)
            <?php $rent = $item->current_rent; ?>
            @if($rent)
                <span class="fa fa-user" data-toggle="tooltip"
                      data-placement="top"
                      title="В аренде у"></span> {{ $rent->user ? $rent->user->fullName : 'Удалён' }}<br>
                <span class="fa fa-calendar" data-toggle="tooltip"
                      data-placement="top"
                      title="Дата начала аренды"></span> {{ $rent->start_date->format('Y-m-d H:i') }}<br>
                <span class="fa fa-clock-o" data-toggle="tooltip"
                      data-placement="top"
                      title="Дата окончания аренды"></span> {{ $rent->end_date->format('Y-m-d H:i') }}<br>
                <span class="fa fa-envelope-open-o" data-toggle="tooltip"
                      data-placement="top"
                      title="Открытий за аренду"></span> {{ $rent->opens_count }}
            @endif
        @elseif($item->status === \App\Models\Cell::ON_SERVICE)
            <span class="badge badge-danger">На обслуживании</span>
        @else
            <span class="badge badge-success">Свободна</span>
        @endif
    </td>
    <td> {{ $item->opens_total }} </td>
    <td>
        <form id="reset-cell-{{ $item->id }}" method="post" action="{{ route("admin::cell.reset.opens", $item) }}">
            {{ csrf_field() }}
        </form>

        <button class="btn btn-xs btn-primary" data-id="reset-cell-{{ $item->id }}" data-form-submit="{{ json_encode(['target' => "#reset-cell-{$item->id}", 'confirm' => true, 'buttonText' => 'Да, сбросить!']) }}">
            <span class="fa fa-undo"></span>
        </button>
        <a href="{{ route('admin::cell.show', $item) }}" class="btn btn-xs btn-info">
            <span class="fa fa-eye"></span>
        </a>
    </td>
</tr>
