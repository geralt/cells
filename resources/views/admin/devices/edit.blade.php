@extends('layouts.admin')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><a href="{{ route('admin::devices') }}"><i
                                    class="fa fa-server"></i> Устройства</a> / Редактировать</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">

                                @include('admin.partials.errors')

                                <form action="{{ route('admin::devices.update', $device) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="form-group">
                                        <label for="mac">Заводской номер</label>
                                        <input type="text" name="mac" class="form-control input-lg" id="mac"
                                               placeholder="01:01:01:01:01:01:01:01" value="{{ old('mac', $device->mac) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="person">Адрес</label>
                                        <select class="form-control" name="place_id">
                                            @foreach($places as $place)
                                                <option value="{{ $place->id }}" @if($place->id == $device->place_id) selected @endif>{{ $place->address }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="company">Требуемая температура</label>
                                        <select class="form-control" name="target_temp">
                                            @foreach($possibleTemps as $temp)
                                                <option value="{{ $temp }}" @if($temp == $device->target_temp) selected @endif>{{ $temp }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="cells_count">Количество ячеек</label>
                                        <input name="cells_count" id="cells_count" class="form-control" value="{{ $device->cells_count }}" readonly="readonly">
                                    </div>

                                    <div class="form-group">
                                        <label for="company">Ip адрес (включая порт)</label>
                                        <input type="text" class="form-control" value="{{ old('ip_address', $device->ip_address) }}" name="ip_address" placeholder="http://127.0.0.1:8080">
                                    </div>

                                    <div class="form-group">
                                        <label for="token">Токен</label>
                                        <input type="text" class="form-control" value="{{ $device->token }}" name="token" id="token" readonly>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
