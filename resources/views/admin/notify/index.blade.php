@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-server" aria-hidden="true"></i>
                    Отправить уведомление
                </span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">

                        @include('admin.partials.errors')

                        <form action="{{ route('admin::notify.send') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="user_id">Пользователь</label>
                                <select id="select2" class="form-control form-control_select" name="user_id"></select>
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="sendToAll" name="all">
                                <label class="form-check-label" for="sendToAll">Отослать всем</label>
                            </div>

                            <div class="form-group">
                                <label for="sms">Текст сообщения</label>
                                <textarea id="sms" name="text" class="form-control">{{ old('text') }}</textarea>
                                <div class="pull-right" id="count" style="color: #ced4da">Введено символов: 0</div>
                            </div>


                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">
                                    Отправить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('document').ready(function () {
            $('#sendToAll').click(function () {
                if ($(this).is(':checked')) {
                    $('#select2').attr('disabled', 'disabled')
                } else {
                    $('#select2').attr('disabled', false)
                }

            });

            $('#select2').select2({
                theme: "bootstrap",
                ajax: {
                    url: '{{ route('admin::users.available') }}',
                    dataType: 'json',
                    type: 'POST',
                    data: function (params) {
                        return {
                            _token: '{{ csrf_token() }}',
                            q: params.term,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.first_name + ' ' + item.last_name,
                                    id: item.id,
                                    data: item
                                };
                            })
                        };
                    },
                    cache: true
                },
            });

            $('#sms').keyup(function(){
                var count = $(this).val().length;
                $('#count').html('Введено символов: ' + count)
            });
        });
    </script>
@endpush
