@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-folder-open" aria-hidden="true"></i>
                    Логи аренды
                </span>
            </div>
            <div class="card-body">
                @include('admin.rents.index.filter')
                <br>

                <div class="row">
                    <div class="col-md-12">
                        @if($rents->count() > 0)
                            <table class="table table-striped">
                                @include('admin.rents.index.head')
                                <tbody>
                                @foreach($rents as $item)
                                    @include('admin.rents.index.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
