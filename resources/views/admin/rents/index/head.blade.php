<thead>
<tr>
    <th>Место</th>
    <th>Телефон</th>
    <th><a href="{{ $sorter->route(\Route::currentRouteName(), 'name') }}">ФИО{!! $sorter->fa('name') !!}</a></th>
    <th>Номер устройства</th>
    <th>Ячейка</th>
    <th><a href="{{ $sorter->route(\Route::currentRouteName(), 'opens_count') }}">Количество открытий{!! $sorter->fa('opens_count') !!}</a></th>
    <th><a href="{{ $sorter->route(\Route::currentRouteName(), 'start_date') }}">Начало{!! $sorter->fa('start_date') !!}</a></th>
    <th><a href="{{ $sorter->route(\Route::currentRouteName(), 'end_date') }}">Конец{!! $sorter->fa('end_date') !!}</a></th>
    <th><a href="{{ $sorter->route(\Route::currentRouteName(), 'hours') }}">Количество часов{!! $sorter->fa('hours') !!}</a></th>
</tr>
</thead>
