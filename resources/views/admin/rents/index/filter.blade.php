<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin::rents') }}" method="get">
                <div class="input-group">
                    <label for="phone" class="filter-label">Телефон</label>
                    <input type="text" id="phone" class="form-control form-control-sm filter-el" name="phone"
                           value="{{ old('phone', request()->phone) }}">


                    <label for="start_date">Дата начала аренды &nbsp;</label>
                    <input type="text" name="start_date" class="form-control input-lg"
                           id="start_date" value="{{ old('start_date', request()->start_date) }}" style="max-width: 500px;">
                    &nbsp;

                    <button class="btn btn-outline-secondary btn-sm filter-button" ><span class="fa fa-filter"></span> Фильтровать</button>
                    <a href="{{ route('admin::rents') }}" class="btn btn-outline-secondary btn-sm filter-button" style="padding-top: 7px;"><span class="fa fa-times"></span> Отмена</a>
                </div><!-- /input-group -->
            </form>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('document').ready(function () {
            jQuery.datetimepicker.setLocale('ru');
            $('#start_date').datetimepicker({
                i18n:{
                    ru: window.ruLocalization
                },
                timepicker:true,
                showSecond: false,
                formatDate:'Y-m-d',
                formatTime: 'H:i',
                format: 'Y-m-d H:i',
            });
        });
    </script>
@endpush
