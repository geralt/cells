<tr>
    <td>{{ $item->cell && $item->cell->device && $item->cell->device->place ? $item->cell->device->place->address : 'Удалено' }}</td>
    <td>{{ $item->user ? $item->user->phone : 'Удалено' }}</td>
    <td>{{ $item->user ? $item->user->fullName : 'Удалено' }}</td>
    <td>{{ $item->cell && $item->cell->device ? $item->cell->device->mac : 'Удалено' }}</td>
    <td>{{ $item->cell ? $item->cell->number : 'Удалено' }}</td>
    <td>{{ $item->opens_count }}</td>
    <td>{{ $item->start_date->format('Y-m-d H:i') }}</td>
    <td>{{ $item->end_date->format('Y-m-d H:i') }}</td>
    <td>{{ $item->diff }}</td>
</tr>

@if($item->extendRents->count() > 0)
    @foreach($item->extendRents as $extend)
        <tr style="background-color: #f3c9c7">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Продление от {{ $extend->created_at->format('Y-m-d H:i') }}</td>
            <td>{{ $extend->extend_from->format('Y-m-d H:i') }}</td>
            <td>{{ $extend->extend_from->addHours($extend->hours)->format('Y-m-d H:i') }}</td>
            <td>{{ $extend->hours }}</td>
        </tr>
    @endforeach
@endif
