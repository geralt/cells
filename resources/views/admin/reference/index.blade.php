@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-server" aria-hidden="true"></i>
                    Справочная информация
                </span>
                <a href="{{ route('admin::reference.create') }}" class="pull-right"><i class="fa fa-plus"></i> Новое устройство</a>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5>Загрузка pdf файлов для политики конфиденциальности и условий эксплуатации</h5>
                        <br>
                    </div>
                    <div class="col-md-12">
                    <form enctype="multipart/form-data" method="post" action="{{ route('admin::upload.pdf') }}">
                        {{ csrf_field() }}
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile" name="privacy">
                            <label class="custom-file-label" for="customFile">Privacy</label>
                        </div>
                        {{ csrf_field() }}
                        <div class="custom-file" style="margin-top: 5px;">
                            <input type="file" class="custom-file-input" id="customFile2" name="terms">
                            <label class="custom-file-label" for="customFile2">Terms</label>
                        </div>
                        <div>
                            <button class="btn btn-primary" style="margin-top: 10px;">Сохранить</button>
                        </div>
                    </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        @if($references->count() > 0)
                            <table class="table">
                                @include('admin.reference.index.head')
                                <tbody>
                                @foreach($references as $item)
                                    @include('admin.reference.index.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                            {{ $references->links() }}
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#customFile').on('change',function(e){
            var fileName = document.getElementById("customFile").files[0].name;
            var nextSibling = e.target.nextElementSibling
            nextSibling.innerText = fileName
        })
        $('#customFile2').on('change',function(e){
            var fileName = document.getElementById("customFile2").files[0].name;
            var nextSibling = e.target.nextElementSibling
            nextSibling.innerText = fileName
        })
    </script>
@endpush
