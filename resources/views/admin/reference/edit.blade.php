@extends('layouts.admin')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><a href="{{ route('admin::reference') }}"><i
                                    class="fa fa-question-circle"></i> Справочная информация</a> / Редактировать</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">

                                @include('admin.partials.errors')

                                <form action="{{ route('admin::reference.update', $reference->place_id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="form-group">
                                        <label for="text">Текст</label>
                                        <textarea class="form-control input-lg" id="text"
                                                  name="text">{{ old('text', $reference->text) }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="person">Адрес</label>
                                        <select class="form-control" name="place_id" readonly="readonly">
                                            <option value="0" @if($reference->place_id == 0) selected @endif >Без адреса</option>
                                            @foreach($places as $place)
                                                <option value="{{ $place->id }}" @if($reference->place_id == $place->id) selected @endif >{{ $place->address }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </form>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
