<tbody>
<tr>
    <td>{{ $item->place_id ? $item->place->address : 'Без адреса' }}</td>
    <td>{{ \Illuminate\Support\Str::words($item->text, 7, ' ...') }}</td>
    <td>
        <form id="delete-reference-{{ $item->place_id }}" method="post" action="{{ route("admin::reference.destroy", $item->place_id) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

        <a href="{{ route("admin::reference.edit", $item->place_id) }}" class="btn btn-xs btn-warning">
            <span class="fa fa-pencil"></span>
        </a>
        <button class="btn btn-xs btn-danger" data-id="delete-reference-{{ $item->place_id }}" data-form-submit="{{ json_encode(['target' => "#delete-reference-{$item->place_id}", 'confirm' => true]) }}">
            <span class="fa fa-trash"></span>
        </button>
    </td>
</tr>
</tbody>
