@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    Клиенты
                </span>
                <a href="{{ route('admin::users.create') }}" class="pull-right" style="margin: 0 5px;"><i class="fa fa-plus"></i> Новый пользователь</a>
            </div>
            <div class="card-body">
                @include('admin.users.index.filter')
                <br>

                <div class="row">
                    <div class="col-md-12">
                        @if($users->count() > 0)
                            <table class="table">
                                @include('admin.users.index.head')
                                <tbody>
                                @foreach($users as $item)
                                    @include('admin.users.index.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                            {{ $users->links() }}
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
