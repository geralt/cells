@extends('layouts.admin')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><a href="{{ route('admin::users') }}"><i
                                    class="fa fa-user"></i> Клиенты</a> / Редактировать</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">

                                @include('admin.partials.errors')

                                <form action="{{ route('admin::users.update', $user) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="form-group">
                                        <label for="first_name">Имя</label>
                                        <input type="text" name="first_name" class="form-control input-lg" id="first_name"
                                               placeholder="Иван" value="{{ old('first_name', $user->first_name) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="patronymic">Отчество</label>
                                        <input type="text" name="patronymic" class="form-control input-lg" id="patronymic"
                                               placeholder="Иванович" value="{{ old('patronymic', $user->patronymic) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="last_name">Фамилия</label>
                                        <input type="text" name="last_name" class="form-control input-lg" id="last_name"
                                               placeholder="Иванов" value="{{ old('last_name', $user->last_name) }}">
                                    </div>

                                    @if(user()->role === 'superadmin')
                                        <div class="form-group">
                                            <label for="person">Роль</label>
                                            <select class="form-control" name="role">
                                                <option value="user" @if($user->role == 'user') selected @endif>Пользователь</option>
                                                <option value="admin" @if($user->role == 'admin') selected @endif>Админ</option>
                                            </select>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="person">Адрес</label>
                                        <?php
                                            $_places = [];
                                            foreach ($user->places as $userPlace) {
                                                $_places[] = $userPlace->id;
                                            }
                                        ?>
                                        <select class="form-control selectpicker" name="place_ids[]" multiple data-live-search="true">
                                            @foreach($places as $place)
                                                <option value="{{ $place->id }}" @if(in_array($place->id, $_places)) selected @endif>{{ $place->address }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Телефон</label>
                                        <input type="text" name="phone" class="form-control input-lg" id="phone"
                                               placeholder="79287771234" value="{{ old('phone', $user->phone) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" class="form-control input-lg" id="email"
                                               placeholder="example@gmail.com" value="{{ old('email', $user->email) }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Пароль</label>
                                        <input type="password" name="password" class="form-control input-lg" id="password"
                                               value="{{ old('email') }}">
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="verify" name="verify"
                                            @if($user->phone_verified_at) checked @endif>
                                            <label class="form-check-label" for="verify">
                                                @if($user->phone_verified_at)
                                                    Верифицирован
                                                @else
                                                    Верифицировать
                                                @endif
                                            </label>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </form>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
