<tr>
    <?php
        $places = [];
        foreach ($item->places as $userPlace) {
            $places[] = $userPlace->address;
        }
    ?>

    <td>{{ $item->id }}</td>
    <td>{{ $item->full_name }}</td>
    <td>{{ count($places) ? implode(",", $places) : '-' }}</td>
    <td>{{ $item->email }}</td>
    <td>{{ $item->phone }}</td>
    <td>
        @if($item->phone_verified_at)
            <span class="fa fa-check" style="color: green"></span>
        @else
            <span class="fa fa-times" style="color: red"></span>
        @endif
    </td>
    <td>
        <form id="delete-user-{{ $item->id }}" method="post" action="{{ route("admin::users.destroy", $item) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

        <a href="{{ route("admin::users.edit", $item) }}" class="btn btn-xs btn-warning">
            <span class="fa fa-pencil"></span>
        </a>
        <button class="btn btn-xs btn-danger" data-id="delete-user-{{ $item->id }}" data-form-submit="{{ json_encode(['target' => "#delete-user-{$item->id}", 'confirm' => true]) }}">
            <span class="fa fa-trash"></span>
        </button>
    </td>
</tr>
