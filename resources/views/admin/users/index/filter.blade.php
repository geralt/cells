<div class="well well-sm">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin::users') }}" method="get">
                <div class="input-group">
                    <label for="address" class="filter-label">Фамилия</label>
                    <input type="text" id="las_name" class="form-control form-control-sm filter-el" name="last_name"
                           value="{{ old('last_name', request()->last_name) }}">

                    <label for="address" class="filter-label">Имя</label>
                    <input type="text" id="name" class="form-control form-control-sm filter-el" name="first_name"
                           value="{{ old('first_name', request()->first_name) }}">

                    <label for="company" class="filter-label">Телефон</label>
                    <input type="text" id="company" class="form-control form-control-sm filter-el" name="phone"
                           value="{{ old('phone', request()->phone) }}">

                    <button class="btn btn-outline-secondary btn-sm filter-button" ><span class="fa fa-filter"></span> Фильтровать</button>
                    <a href="{{ route('admin::users') }}" class="btn btn-outline-secondary btn-sm filter-button" style="padding-top: 7px;"><span class="fa fa-times"></span> Отмена</a>
                </div><!-- /input-group -->
            </form>
        </div>
    </div>
</div>
