<nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ route('admin::home') }}">
      <img src="{{ asset('admin/imgs/brand.svg') }}" width="30" height="30" class="d-inline-block align-top" alt="{{ config('app.name') }}">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_content">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbar_content">
        @if (is_admin())
        <ul class="navbar-nav mr-auto">
            <li class="nav-item mx-1 @if(request()->segment(1) === 'places') active @endif">
               <a class="nav-link btn btn-light" href="{{ route('admin::places') }}"><i class="fa fa-map-marker"></i> Места</a>
            </li>

            <li class="nav-item mx-1 @if(request()->segment(1) === 'users' && request()->segment(2) !== 'phone-address') active @endif">
                <a class="nav-link btn btn-light" href="{{ route('admin::users') }}"><i class="fa fa-user"></i> Клиенты</a>
            </li>

            <li class="nav-item mx-1 @if(request()->segment(1) === 'users' && request()->segment(2) === 'phone-address') active @endif">
                <a class="nav-link btn btn-light" href="{{ route('admin::users.show.phone.address') }}"><i class="fa fa-home"></i> Адреса клиентов</a>
            </li>

            <li class="nav-item mx-1 @if(request()->segment(1) === 'devices' || request()->segment(1) === 'cells') active @endif">
                <a class="nav-link btn btn-light" href="{{ route('admin::devices') }}"><i class="fa fa-server"></i> Устройства</a>
            </li>

            <li class="nav-item mx-1 @if(request()->segment(1) === 'rents') active @endif">
                <a class="nav-link btn btn-light" href="{{ route('admin::rents') }}"><i class="fa fa-folder-open"></i> Аренда</a>
            </li>

            <li class="nav-item mx-1 @if(request()->segment(1) === 'notify') active @endif">
                <a class="nav-link btn btn-light" href="{{ route('admin::notify') }}"><i class="fa fa-comments-o"></i> SMS уведомление</a>
            </li>

            <li class="nav-item mx-1 @if(request()->segment(1) === 'reference') active @endif">
                <a class="nav-link btn btn-light" href="{{ route('admin::reference') }}"><i class="fa fa-question-circle"></i> Справочная информация</a>
            </li>
      </ul>

        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
              <i class="fa fa-fw fa-user-o"></i> {{ user()->name }}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="{{ url('/admin/logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
            </div>
          </li>
        </ul>
      @endif
    </div>
  </div>
</nav>
