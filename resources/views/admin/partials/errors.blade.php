@if($errors->all())
    <span class="form-text alert alert-danger">
         <ul>
             @foreach($errors->all() as $error)
                 <li>{{ $error }}</li>
             @endforeach
         </ul>
     </span>
@endif