@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    Адреса клиентов
                </span>
                <span class="pull-right">
                    <a href="{{ route('admin::import.show.phone.address.import') }}"><i class="fa fa-download"></i> Импорт</a>
                </span>
            </div>
            <div class="card-body">
                @include('admin.partials.errors')
                <h5>Добавить адрес:</h5>
                <br>
                <form method="post" action="{{ route('admin::users.save.phone.address') }}">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <input type="text" name="phone" class="form-control input-lg" id="phone"
                                   placeholder="79287771234" value="{{ old('phone') }}">
                        </div>
                        <div class="form-group col-md-4">
                            <select class="form-control" name="place_id">
                                @foreach($places as $place)
                                    <option value="{{ $place->id }}">{{ $place->address }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <button class="btn btn-primary">
                                Сохранить
                            </button>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-md-12">
                        @if($addresses->count() > 0)
                            <table class="table">
                                @include('admin.phone-addresses.address.head')
                                <tbody>
                                @foreach($addresses as $item)
                                    @include('admin.phone-addresses.address.item', $item)
                                @endforeach
                                </tbody>
                            </table>
                            {{ $addresses->links() }}
                        @else
                            @include('admin.partials.noitems')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
