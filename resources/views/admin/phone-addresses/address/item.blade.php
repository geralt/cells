<tr>
    <td>{{ $item->phone }}</td>
    <td>{{ $item->place->address ?? '-' }}</td>
    <td>
        <form id="delete-address-{{ $item->phone }}" method="post" action="{{ route("admin::users.remove.phone.address", $item->phone) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        </form>

        <button class="btn btn-xs btn-danger" data-id="delete-address-{{ $item->phone }}" data-form-submit="{{ json_encode(['target' => "#delete-address-{$item->phone}", 'confirm' => true]) }}">
            <span class="fa fa-trash"></span>
        </button>
    </td>
</tr>
