<thead>
<tr>
    <th><a href="{{ $sorter->route(\Route::currentRouteName(), 'phone') }}">Телефон{!! $sorter->fa('phone') !!}</a></th>
    <th><a href="{{ $sorter->route(\Route::currentRouteName(), 'address') }}">Адрес{!! $sorter->fa('address') !!}</a></th>
    <th></th>
</tr>
</thead>
