@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <div class="card card-default">
            <div class="card-header">
                <span class="card-title">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    Адреса клиентов / импорт
                </span>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('admin::import.phone.address') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <input type="file" name="addresses" class="form-control-file input-lg" id="phone" placeholder="Выберите файл">
                        </div>
                        <div class="form-group col-md-4">
                            <button class="btn btn-primary">
                                <i class="fa fa-download"></i> Импортировать
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
