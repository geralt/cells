<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 46px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .glow-button {
            text-decoration: none;
            display: inline-block;
            padding: 15px 30px;
            margin: 10px 20px;
            border-radius: 10px;
            box-shadow: 0 0 40px 40px #737c92 inset, 0 0 0 0 #737c93;
            font-family: 'Montserrat', sans-serif;
            font-weight: bold;
            letter-spacing: 2px;
            color: white;
            transition: .15s ease-in-out;
        }
        .glow-button:hover {
            box-shadow: 0 0 10px 0 #737c92 inset, 0 0 10px 4px #737c93;
            color: #737c93;
        }

        .loader-wrapper {
            display: none;
        }

        .cssload-loader {
            position: relative;
            left: calc(50% - 31px);
            width: 100px;
            height: 100px;
            margin: 130px 0;
            perspective: 780px;
        }

        .cssload-inner {
            position: absolute;
            width: 100%;
            height: 100%;
            box-sizing: border-box;
            -o-box-sizing: border-box;
            -ms-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            border-radius: 50%;
            -o-border-radius: 50%;
            -ms-border-radius: 50%;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
        }
        .cssload-inner.cssload-one {
            left: 0%;
            top: 0%;
            animation: cssload-rotate-one 1.15s linear infinite;
            -o-animation: cssload-rotate-one 1.15s linear infinite;
            -ms-animation: cssload-rotate-one 1.15s linear infinite;
            -webkit-animation: cssload-rotate-one 1.15s linear infinite;
            -moz-animation: cssload-rotate-one 1.15s linear infinite;
            border-bottom: 3px solid #5C5EDC;
        }
        .cssload-inner.cssload-two {
            right: 0%;
            top: 0%;
            animation: cssload-rotate-two 1.15s linear infinite;
            -o-animation: cssload-rotate-two 1.15s linear infinite;
            -ms-animation: cssload-rotate-two 1.15s linear infinite;
            -webkit-animation: cssload-rotate-two 1.15s linear infinite;
            -moz-animation: cssload-rotate-two 1.15s linear infinite;
            border-right: 3px solid rgba(76, 70, 101, 0.99);
        }
        .cssload-inner.cssload-three {
            right: 0%;
            bottom: 0%;
            animation: cssload-rotate-three 1.15s linear infinite;
            -o-animation: cssload-rotate-three 1.15s linear infinite;
            -ms-animation: cssload-rotate-three 1.15s linear infinite;
            -webkit-animation: cssload-rotate-three 1.15s linear infinite;
            -moz-animation: cssload-rotate-three 1.15s linear infinite;
            border-top: 3px solid #e9908a;
        }

        @keyframes cssload-rotate-one {
            0% {
                transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
            }
            100% {
                transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
            }
        }
        @-webkit-keyframes cssload-rotate-one {
            0% {
                -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
            }
            100% {
                -webkit-transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
            }
        }
        @keyframes cssload-rotate-two {
            0% {
                transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
            }
            100% {
                transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
            }
        }
        @-webkit-keyframes cssload-rotate-two {
            0% {
                -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
            }
            100% {
                -webkit-transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
            }
        }
        @keyframes cssload-rotate-three {
            0% {
                transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
            }
            100% {
                transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
            }
        }
        @-webkit-keyframes cssload-rotate-three {
            0% {
                -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
            }
            100% {
                -webkit-transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
            }
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            {{ $cell->device->place->title . ', ' . $cell->device->place->address }}
            <br>
            ячейка №{{ $cell->number }}
        </div>

        <div class="loader-wrapper">
            Выполняется запрос...
            <div class="cssload-loader">
                <div class="cssload-inner cssload-one"></div>
                <div class="cssload-inner cssload-two"></div>
                <div class="cssload-inner cssload-three"></div>
            </div>
        </div>

        <div class="links">
            <form method="post" action="{{ route('cell.open', $token) }}">
                {{ csrf_field() }}
                <button class="glow-button" type="submit">Открыть</button>
            </form>
        </div>
    </div>
</div>
<script src="{{ asset('admin/js/app.js') }}"></script>
<script>
    $('.glow-button').click(function () {
        $(this).hide();
        $('.loader-wrapper').show();
    });
</script>
</body>
</html>
