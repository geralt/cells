<?php

use Illuminate\Support\Facades\Route;

Route::get('page/{slug}', 'PagesController@show');

//TODO should be deleted on next iteration of deploy
Route::get('privacy', function () {
    return redirect()->to('/page/privacy');
});
Route::get('terms', function () {
    return redirect()->to('/page/terms');
});

Route::group([], function() {
    //Log In / Log Out
    Route::get('/admin/login', 'AuthController@showLogin');
    Route::post('/admin/login', 'AuthController@login')->name('admin.login');
    Route::get('/admin/logout', 'AuthController@logout')->name('logout');
});

Route::get('/cell/{token}', 'CellController@showOpenDialog')->name('cell.show.open.dialog');
Route::post('/cell/{token}/open', 'CellController@open')->name('cell.open');

Route::group(['middleware' => ['is_admin'], 'namespace' => 'Admin', 'as' => 'admin::'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    //Places
    Route::get('/places', 'PlacesController@index')->name('places');
    Route::get('/places/create', 'PlacesController@create')->name('places.create');
    Route::post('/places/store', 'PlacesController@store')->name('places.store');
    Route::get('/places/{id}/edit', 'PlacesController@edit')->name('places.edit');
    Route::put('/places/{id}/update', 'PlacesController@update')->name('places.update');
    Route::delete('/places/{id}', 'PlacesController@destroy')->name('places.destroy');
    //Users
    Route::get('/users', 'UsersController@index')->name('users');
    Route::get('/users/create', 'UsersController@create')->name('users.create');
    Route::post('/users/store', 'UsersController@store')->name('users.store');
    Route::get('/users/{id}/edit', 'UsersController@edit')->name('users.edit');
    Route::put('/users/{id}/update', 'UsersController@update')->name('users.update');
    Route::delete('/users/{id}', 'UsersController@destroy')->name('users.destroy');
    Route::post('/users/available', 'UsersController@available')->name('users.available');
    //Devices
    Route::get('/devices', 'DevicesController@index')->name('devices');
    Route::get('/devices/create', 'DevicesController@create')->name('devices.create');
    Route::post('/devices/store', 'DevicesController@store')->name('devices.store');
    Route::get('/devices/{id}/edit', 'DevicesController@edit')->name('devices.edit');
    Route::put('/devices/{id}/update', 'DevicesController@update')->name('devices.update');
    Route::delete('/devices/{id}', 'DevicesController@destroy')->name('devices.destroy');
    Route::get('/devices/{id}/temp/{period}', 'DevicesController@getTemperatures');
    Route::post('/devices/{id}/temp/download', 'DevicesController@downloadTemperatures');

    Route::get('devices/{id}/cells-sizes', 'CellSizesController@showForm')->name('cells.choose.size');
    Route::post('devices/{id}/cells-sizes', 'CellSizesController@saveSizes')->name('cells.store.size');
    //Cells
    Route::get('devices/{id}/cells', 'DevicesController@cells')->name('cells');
    Route::get('cells/{id}/show', 'CellController@show')->name('cell.show');
    Route::post('cells/{id}/reset-opens', 'CellController@reset')->name('cell.reset.opens');
    Route::put('cells/{id}/update', 'CellController@update')->name('cell.update');
    Route::get('cells/{id}/cancel-rent', 'CellController@cancelRent')->name('cell.cancel.rent');
    Route::get('cells/{id}/open', 'CellController@openDoor')->name('cell.open');
    Route::get('cells/{id}/show-add-rent', 'CellController@showAddRent')->name('cell.show.add.rent');
    Route::post('cells/{id}/add-rent', 'CellController@addRent')->name('cell.add.rent');

    Route::get('cells/sizes', 'CellSizesController@index')->name('cells.sizes');
    Route::get('cells/sizes/create', 'CellSizesController@showCreate')->name('cells.sizes.create');
    Route::post('cells/sizes/store', 'CellSizesController@store')->name('cells.sizes.store');
    Route::get('cells/sizes/{id}/edit', 'CellSizesController@edit')->name('cells.sizes.edit');
    Route::put('cells/sizes/{id}/update', 'CellSizesController@update')->name('cells.sizes.update');
    Route::delete('cells/sizes/destroy/{id}', 'CellSizesController@destroy')->name('cells.sizes.destroy');
    //Notify
    Route::get('notify/show', 'NotifyController@show')->name('notify');
    Route::post('notify/send', 'NotifyController@send')->name('notify.send');
    //Reference
    Route::get('reference', 'ReferenceController@index')->name('reference');
    Route::get('/reference/create', 'ReferenceController@create')->name('reference.create');
    Route::post('/reference/store', 'ReferenceController@store')->name('reference.store');
    Route::get('/reference/{place_id?}/edit', 'ReferenceController@edit')->name('reference.edit');
    Route::put('/reference/{place_id?}/update', 'ReferenceController@update')->name('reference.update');
    Route::delete('/reference/{place_id?}', 'ReferenceController@destroy')->name('reference.destroy');
    Route::post('reference/upload/pdf', 'ReferenceController@uploadPdf')->name('upload.pdf');
    //Rents
    Route::get('rents', 'RentsController@index')->name('rents');
    Route::get('rents/all', 'RentsController@all')->name('rents.all');
    //Phone Addresses
    Route::get('/users/phone-address', 'PhoneAddressController@showAddresses')->name('users.show.phone.address');
    Route::post('/users/phone-address', 'PhoneAddressController@saveAddress')->name('users.save.phone.address');
    Route::delete('/users/{phone}/phone-address', 'PhoneAddressController@removePhoneAddress')->name('users.remove.phone.address');
    Route::get('users/phone-address/show/import', 'PhoneAddressController@showImport')->name('import.show.phone.address.import');
    Route::post('users/phone-address/import', 'PhoneAddressController@import')->name('import.phone.address');
});
