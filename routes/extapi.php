<?php

Route::group(['namespace' => 'ExtApi'], function () {
    Route::group(['prefix' => 'device'], function () {
        Route::post('temp', 'DeviceController@temp');
        Route::post('statuses', 'DeviceController@statuses');
    });
});
