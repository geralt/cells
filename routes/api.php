<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Api'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('code/check', 'AuthController@checkCode');
    Route::post('code/resend', 'AuthController@resendCode');
    Route::get('forgot/password', 'AuthController@getRecoveryCode');
    Route::post('change/password', 'AuthController@changePassword');
});

Route::group(['middleware' => 'api.access_token', 'namespace' => 'Api'], function () {
    Route::get('reference', 'ReferenceController@getReference');
    Route::get('cells', 'CellsController@getList');
    Route::get('cells/my', 'CellsController@myCells');
    Route::post('cells/rent', 'CellsController@rentCell');
    Route::put('cells/{id}/extend-rent', 'CellsController@extendRent');
    Route::put('cells/{id}/open', 'CellsController@openDoor');
    Route::put('cells/{id}/cancel-rent', 'CellsController@cancelRent');
    Route::get('cells/{id}/door-status', 'CellsController@getDoorStatus');
    Route::get('cells/{id}/get-access', 'CellsController@generateAccessToken');
    Route::put('user/device_id', 'UserController@setFcmToken');
    Route::get('user/allowed-cells-sizes', 'UserController@getPossibleSizes');
    Route::get('devices', 'DevicesController@getList');
});
