<?php

namespace App\Http\Requests\Admin\Devices;

use Illuminate\Foundation\Http\FormRequest;

class StoreDeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mac' => 'required',
            'place_id' => 'required',
            'target_temp' => 'required',
            'cells_count' => 'required|numeric|min:1|max:10',
        ];
    }
}
