<?php

namespace App\Http\Requests\Admin\Cell;

use Illuminate\Foundation\Http\FormRequest;

class AddRent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
    }
}
