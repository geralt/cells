<?php

namespace App\Http\Requests;

use App\Services\ApiResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class Request extends FormRequest
{
    protected $response;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->response = new ApiResponse();
    }

    protected function failedValidation(Validator $validator)
    {
        return $this->messages();
    }

    public function messages()
    {
        return [
            'statusCode' => 400,
            'message' => 'failure',
            'description' => 'Validation error',
            'data' => new \stdClass()
        ];
    }
}
