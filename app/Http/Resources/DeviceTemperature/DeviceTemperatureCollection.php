<?php

namespace App\Http\Resources\DeviceTemperature;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DeviceTemperatureCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'temps' => DeviceTemperatureResource::collection($this->collection),
        ];
    }
}
