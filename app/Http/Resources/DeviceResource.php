<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeviceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'mac' => $this->mac,
            'current_temp' => $this->current_temp,
            'target_temp' => $this->target_temp,
            'place' => $this->place->address,
            'latitude' => $this->place->latitude,
            'longitude' => $this->place->longitude
        ];
    }
}
