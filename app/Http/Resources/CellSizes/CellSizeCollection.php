<?php

namespace App\Http\Resources\CellSizes;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CellSizeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'sizes' => CellSizeResource::collection($this->collection),
        ];
    }
}
