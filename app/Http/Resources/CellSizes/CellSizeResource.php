<?php

namespace App\Http\Resources\CellSizes;

use Illuminate\Http\Resources\Json\JsonResource;

class CellSizeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'width' => $this->width,
            'height' => $this->height,
            'depth' => $this->depth
        ];
    }
}
