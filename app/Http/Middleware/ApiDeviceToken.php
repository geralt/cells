<?php

namespace App\Http\Middleware;

use App\Models\Device;
use App\Models\User;
use App\Services\ApiResponse;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Log;


class ApiDeviceToken
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-Access-Token');

        if($token == '') {
            return (new ApiResponse())->failure('Unauthorized', 401);
        }

        $device = Device::where('token', $token)->first();

        if(!$device){
            return (new ApiResponse())->failure('Unauthorized', 401);
        }

        $request->device = $device;

        return $next($request);
    }
}
