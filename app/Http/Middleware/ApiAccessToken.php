<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Services\ApiResponse;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Log;


class ApiAccessToken
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-Access-Token');

        if($token == '') {
            return (new ApiResponse())->failure('Unauthorized', 401);
        }

        $user = User::where('token', $token)->first();

        if(!$user || Carbon::now()->gt($user->token_expires) || !$user->phone_verified_at){
            return (new ApiResponse())->failure('Unauthorized', 401);
        }

        $user->updateToken();
        $request->user = $user;

        /*$log = [
            'URI' => $request->getUri(),
            'REQUEST_BODY' => $request->all(),
            'METHOD' => $request->getMethod(),
            'USER_ID' => $user->id
        ];

        Log::info("Incoming request: " . json_encode($log));*/

        return $next($request);
    }
}
