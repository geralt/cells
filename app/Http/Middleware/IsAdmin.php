<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class IsAdmin
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if($user && ($user->role =='admin' || $user->role == 'superadmin')) {
            return $next($request);
        }

        return response('Not allowed', 405);
    }
}
