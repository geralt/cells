<?php

namespace App\Http\Controllers\ExtApi;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Models\DeviceTemperature;
use App\Services\DeviceService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public function temp(Request $request)
    {
        $device = $request->device;
        $body = $request->getContent();
        if($body) {
            $temps = [];
            foreach ($body[0]['sensor'] as $sensorId => $temperature) {
                $temps[] = $temperature;
            }
            if(count($temps) > 0) {
                $now = Carbon::now();
                $maxTemp = max($temps);
                $device->current_temp = $maxTemp;
                $device->answered_at = $now;
                $device->save();

                DeviceTemperature::create([
                    'device_id' => $device->id,
                    'temp' => $maxTemp,
                    'created_at' => $now
                ]);
            }

            return response("OK");
        }

        return response("Empty body", 400);
    }

    public function statuses(Request $request, DeviceService $deviceService)
    {
        $device = $request->device;
        $body = $request->getContent();
        if($body) {
            $cells = $device->cells;
            $cellsStatuses = json_decode($body, true);

            $deviceService->updateDoorsStatus($cells, $cellsStatuses);

            return response("OK");
        }

        return response("Empty body", 400);
    }
}
