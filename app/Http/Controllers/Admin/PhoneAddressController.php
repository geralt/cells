<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\SaveAddressRequest;
use App\Imports\PhoneAddressImport;
use App\Models\PhoneAddress;
use App\Models\Place;
use App\Services\Table\Sorters\ClientAddressesSorter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class PhoneAddressController extends Controller
{
    public function showAddresses()
    {
        $sorter = ClientAddressesSorter::getColumns();
        $addresses = PhoneAddress::getAddresses($sorter);
        $places = Place::all();

        return view('admin.phone-addresses.addresses', compact('addresses', 'places', 'sorter'));
    }

    public function saveAddress(SaveAddressRequest $request)
    {
        try {
            PhoneAddress::create([
                'phone' => $request->get('phone'),
                'place_id' => $request->get('place_id')
            ]);

            return redirect()->route('admin::users.show.phone.address')->with('success', 'Добавлено');
        } catch (\Exception $e) {
            return redirect()->route('admin::users.show.phone.address')->with('error', 'Телефон уже существует');
        }
    }

    public function removePhoneAddress($phone)
    {
        $phoneAddress = PhoneAddress::where('phone', '=', $phone)->first();
        if(!$phoneAddress) {
            return redirect()->route('admin::users.show.phone.address')->with('error', 'Неизвестный номер');
        }

        $phoneAddress->delete();

        return redirect()->route('admin::users.show.phone.address')->with('success', 'Удалено');
    }

    public function showImport()
    {
        return view('admin.phone-addresses.import');
    }

    public function import(Request $request)
    {
        try {
            Excel::import(new PhoneAddressImport, $request->file('addresses'));
            return redirect()->route('admin::users.show.phone.address')->with('success', 'Импорт успешно завершен');
        } catch (\Exception $e) {
            Log::error($e->getMessage() . "\n" . $e->getTraceAsString());
            return redirect()->route('admin::users.show.phone.address')->with('error', 'Ошибка во время импорта');
        }
    }
}
