<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Places\StorePlaceRequest;
use App\Models\Place;
use Illuminate\Http\Request;

class PlacesController extends Controller
{
    public function index(Request $request)
    {
        $address = $request->get('address');
        $company = $request->get('company');

        $places = Place::when($address, function ($query) use($address) {
            $query->where('address', $address);
        })
        ->when($company, function ($query) use($company) {
            $query->where('company', $company);
        })->paginate(50);
        return view('admin.places.index', compact('places'));
    }

    public function create()
    {
        return view('admin.places.create');
    }

    public function store(StorePlaceRequest $request)
    {
        Place::create($request->all());

        return redirect()->route('admin::places')->with('success', 'Место добавлено');
    }

    public function edit($id)
    {
        $place = Place::find($id);

        if(!$place) {
            return redirect()->route('admin::places')->with('error', 'Неизвестное место');
        }

        return view('admin.places.edit', compact('place'));
    }

    public function update($id, StorePlaceRequest $request)
    {
        $place = Place::find($id);

        if(!$place) {
            return redirect()->route('admin::places')->with('error', 'Неизвестное место');
        }

        $place->update($request->all());

        return redirect()->route('admin::places')->with('success', 'Обновлено');
    }

    public function destroy($id)
    {
        $place = Place::find($id);

        if(!$place) {
            return redirect()->route('admin::places')->with('error', 'Неизвестное место');
        }

        $devices = $place->devices;
        if($devices->count() > 0) {
            return redirect()->route('admin::places')->with('error', 'Невозможно удалить место, по адресу ' . $place->address . ' есть активные устройства');
        }

        $place->delete();

        return redirect()->route('admin::places')->with('success', 'Удалено');
    }
}
