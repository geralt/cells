<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Cell\AddRent;
use App\Models\Cell;
use App\Models\Rent;
use App\Models\Size;
use App\Services\DeviceService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CellController extends Controller
{
    public function show($id, DeviceService $deviceService)
    {
        $cell = Cell::with(['device'])->find($id);

        if(is_null($cell)) {
            return redirect()->route('admin::cells')->with('error', 'Неизвестная ячейка');
        }

        $sizes = Size::all();
        $response = $deviceService->request($cell->device->ip_address ."/{$cell->number}/status");

        if($response === '') {
            $doorStatus = 'Нет доступа к устройству';
        } else {
            $cellStatus = json_decode($response, true);
            $doorStatus = $cellStatus[0]['pinOut'] == 1 ? 'Дверь открыта' : 'Дверь закрыта';
        }

        return view('admin.cell.show', compact('cell', 'doorStatus', 'sizes'));
    }

    public function update($id, Request $request)
    {
        $cell = Cell::with('device')->find($id);

        if(is_null($cell)) {
            return redirect()->route('admin::cells')->with('error', 'Неизвестная ячейка');
        }

        $oldSizeId = $cell->size_id;
        $sizeId = (int)$request->get('size_id');

        if($oldSizeId != $sizeId) {
            $cell->size_id = $sizeId;
            $cell->save();

            Log::info('Changed size_id ' . $oldSizeId . ' to ' . $sizeId . ' for cell : '.$cell->device->mac . '/'.$cell->number.' by admin at ' . Carbon::now()->format('Y-m-d H:i:s'));
        }

        $oldStatus = $cell->status;
        $status = $request->get('status');

        if($status !== null && $oldStatus != $status) {
            $status = (int)$status;
            $cell->status = $status;
            $cell->save();

            Log::info('Changed status from ' . $oldStatus . ' to ' . $status . ' for cell : '.$cell->device->mac . '/'.$cell->number.' by admin at ' . Carbon::now()->format('Y-m-d H:i:s'));
        }

        $currentRent = $cell->current_rent;
        $endDate = $request->get('end_date');
        if($endDate) {
            $end_date = Carbon::createFromFormat('Y-m-d H:i', $endDate);

            if($currentRent->end_date->format('Y-m-d H:i') != $end_date->format('Y-m-d H:i')) {
                $currentRent->end_date = $end_date;
                $currentRent->save();

                Log::info('Changed rent end date to ' . $end_date->format('Y-m-d H:i') . 'for cell : '.$cell->device->mac . '/'.$cell->number.' by admin at ' . Carbon::now()->format('Y-m-d H:i:s'));
            }
        }

        return redirect()->route('admin::cell.show', $cell)->with('success', 'Изменено');
    }

    public function openDoor($id, DeviceService $deviceService)
    {
        $cell = Cell::with('device')->find($id);

        if(is_null($cell)) {
            return redirect()->route('admin::cells')->with('error', 'Неизвестная ячейка');
        }

        try {
            $isDoorOpened = $deviceService->openDoor($cell);

            if($isDoorOpened) {
                Log::info('Door opened for cell : '.$cell->device->mac . '/'.$cell->number.' by admin at ' . Carbon::now()->format('Y-m-d H:i:s'));
            }
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
        }

        return redirect()->route('admin::cell.show', $cell)->with('success', 'Сигнал открытия двери отправлен');
    }

    public function cancelRent($id)
    {
        $cell = Cell::find($id);

        if(is_null($cell)) {
            return redirect()->route('admin::cells')->with('error', 'Неизвестная ячейка');
        }

        try {
            $cell->current_rent()->delete();
            $cell->status = Cell::FREE;
            $cell->save();
            Log::info('Rent cancelled by admin for cell : '.$cell->device . '/'.$cell->number.'  at ' . Carbon::now()->format('Y-m-d H:i:s'));
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
        }

        return redirect()->route('admin::cell.show', $cell)->with('success', 'Аренда отменена');
    }

    public function showAddRent($id)
    {
        $cell = Cell::find($id);

        if(is_null($cell)) {
            return redirect()->route('admin::cells')->with('error', 'Неизвестная ячейка');
        }

        return view('admin.cell.add_rent', compact('cell'));
    }

    public function addRent($cellId, AddRent $request)
    {
        $cell = Cell::find($cellId);

        if(!$cell) {
            return redirect()->back()->with('error', 'Невернеая ячейка');
        }

        $rent = Rent::create([
            'user_id' => $request->user_id,
            'cell_id' => $cell->id,
            'start_date' => Carbon::createFromFormat('Y-m-d H:i', $request->start_date),
            'end_date' => Carbon::createFromFormat('Y-m-d H:i', $request->end_date),
        ]);

        $cell->status = Cell::IN_USE;
        $cell->rent_id = $rent->id;
        $cell->save();

        return redirect()->route('admin::cell.show', $cell)->with('success', 'Аренда назначена');
    }

    public function reset($cellId)
    {
        $cell = Cell::find($cellId);
        if(!$cell) {
            return redirect()->back()->with('error', 'Неверная ячейка');
        }

        $cell->opens_total = 0;
        $cell->save();

        return redirect()->back()->with('success', 'Счутчик сброшен');
    }
}
