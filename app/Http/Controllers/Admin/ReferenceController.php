<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Reference\StoreReferenceRequest;
use App\Models\Page;
use App\Models\Place;
use App\Models\Reference;
use Illuminate\Http\Request;

class ReferenceController extends Controller
{
    public function index()
    {
        $references = Reference::paginate(50);
        return view('admin.reference.index', compact('references'));
    }

    public function create()
    {
        $places = Place::get();
        return view('admin.reference.create', compact('places'));
    }

    public function store(StoreReferenceRequest $request)
    {
        Reference::create($request->all());

        return redirect()->route('admin::reference')->with('success', 'Место добавлено');
    }

    public function edit($placeId)
    {
        $reference = Reference::where('place_id', '=', $placeId)->first();

        if(!$reference) {
            return redirect()->route('admin::reference')->with('error', 'Неизвестное место');
        }

        $places = Place::get();

        return view('admin.reference.edit', compact('reference', 'places'));
    }

    public function update($placeId, StoreReferenceRequest $request)
    {
        $reference = Reference::find($placeId);

        if(!$reference) {
            return redirect()->route('admin::reference')->with('error', 'Неизвестное место');
        }

        $reference->text = $request->get('text', '');
        $reference->save();

        return redirect()->route('admin::reference')->with('success', 'Обновлено');
    }

    public function destroy($placeId)
    {
        $reference = Reference::where('place_id', '=', $placeId)->first();

        if(!$reference) {
            return redirect()->route('admin::reference')->with('error', 'Неизвестное место');
        }

        $reference->delete();

        return redirect()->route('admin::reference')->with('success', 'Удалено');
    }

    public function uploadPdf(Request $request)
    {
        if(($request->file('privacy') && $request->file('privacy')->getClientOriginalExtension() != 'pdf')
            || ($request->file('terms') && $request->file('terms')->getClientOriginalExtension() != 'pdf'))
        {
            back()->with('error', 'Неверное расширение');
        }

        if($request->file('privacy')) {
            $uniquePrivacyFileName = uniqid() . '.' . $request->file('privacy')->getClientOriginalExtension();
            $request->file('privacy')->move(storage_path('app/public'), $uniquePrivacyFileName);
        }

        if($request->file('terms')) {
            $uniqueTermsFileName = uniqid() . '.' . $request->file('terms')->getClientOriginalExtension();
            $request->file('terms')->move(storage_path('app/public'), $uniqueTermsFileName);
        }

        if(isset($uniquePrivacyFileName)) {
            $privacy = Page::where('slug', 'privacy')->first();
            if(!$privacy) {
                $privacy = new Page();
                $privacy->slug = 'privacy';
            }
            $privacy->filename = $uniquePrivacyFileName;
            $privacy->save();
        }

        if(isset($uniqueTermsFileName)) {
            $terms = Page::where('slug', 'terms')->first();
            if(!$terms) {
                $terms = new Page();
                $terms->slug = 'terms';
            }
            $terms->filename = $uniqueTermsFileName;
            $terms->save();
        }

        return redirect()->back()->with('success', 'Файл успешно загружен');
    }
}
