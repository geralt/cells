<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rent;
use App\Services\Table\Sorters\RentSorter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RentsController extends Controller
{
    public function index(Request $request)
    {
        $sorter = RentSorter::getColumns();
        $phone = $request->get('phone');
        $dateStart = $request->get('start_date');

        $rents = Rent::buildReport($sorter, $phone, $dateStart)->paginate(50);

        return view('admin.rents.index', compact('rents', 'sorter'));
    }

    public function all(Request $request)
    {
        $sorter = RentSorter::getColumns();
        $phone = $request->get('phone');
        $dateStart = $request->get('start_date');

        $rents = Rent::buildReport($sorter, $phone, $dateStart)->get();

        return view('admin.rents.all', compact('rents', 'sorter'));
    }
}
