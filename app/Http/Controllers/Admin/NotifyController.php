<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Notify\SendSms;
use App\Models\User;
use App\Services\TwilioService;

class NotifyController extends Controller
{
    public function show()
    {
        return view('admin.notify.index');
    }

    public function send(SendSms $request, TwilioService $twilioService)
    {
        $toAll = $request->get('toAll');
        $text = $request->get('text');

        if(!$toAll) {
            $userId = $request->get('user_id');

            $user = User::find($userId);
            if(!$user) {
                return redirect()->back()->with('error', 'Неверный адресат');
            }
            $sent = $twilioService->sendSMS($user->phone, $text);

            if ($sent)
                 return redirect()->back()->with('success', 'Отправлено');
            else
                return redirect()->back()->with('error', 'Ошибка при отправке');
        } else {
            $users = User::where('role', '=', 'user')->get();
            foreach ($users as $user) {
                $twilioService->sendSMS($user->phone, $text);
            }

            return redirect()->back()->with('success', 'Отправлено');
        }
    }
}
