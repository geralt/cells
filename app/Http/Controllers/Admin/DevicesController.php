<?php

namespace App\Http\Controllers\Admin;

use App\Exports\DeviceTemperatures;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Devices\StoreDeviceRequest;
use App\Http\Resources\DeviceTemperature\DeviceTemperatureCollection;
use App\Models\Cell;
use App\Models\Device;
use App\Models\DeviceTemperature;
use App\Models\Place;
use App\Models\Rent;
use App\Services\DeviceService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;


class DevicesController extends Controller
{
    public function index(Request $request)
    {
        $mac = $request->get('mac');
        $address = $request->get('address');

        $devices = Device::with(['cells', 'place'/*, 'temperatures'*/])->when($address, function ($query) use($address) {
            $query->whereHas('place', function ($q) use ($address) {
                $q->where('address', $address);
            });
        })
            ->when($mac, function ($query) use($mac) {
                $query->where('mac', $mac);
            })->paginate(50);

        $totalFree = 0;
        $totalInUse = 0;
        $totalOnService = 0;

        foreach ($devices as $device) {
            $totalFree += $device->cells->filter(function ($item) { return $item->status === Cell::FREE; })->count();
            $totalInUse += $device->cells->filter(function ($item) { return $item->status === Cell::IN_USE; })->count();
            $totalOnService += $device->cells->filter(function ($item) { return $item->status === Cell::ON_SERVICE; })->count();
        }

        return view('admin.devices.index', compact('devices', 'totalInUse', 'totalOnService', 'totalFree'));
    }

    public function create()
    {
        $places = Place::all();
        $possibleTemps = config('devices.possible_temp');
        $cellsVariants = config('devices.cells_count');

        return view('admin.devices.create', compact('places', 'possibleTemps', 'cellsVariants'));
    }

    public function store(StoreDeviceRequest $request)
    {
        $data = $request->all();
        $device = Device::create($data);

        $cells = [];
        foreach(range(1, $data['cells_count']) as $item) {
            $cells[] = [
                'device_id' => $device->id,
                'number' => $item,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

        DB::table('cells')->insert($cells);

        return redirect()->route('admin::cells.choose.size', $device->id)->with('success', 'Устройство добавлено');
    }

    public function edit($id)
    {
        $device = Device::find($id);

        if(!$device) {
            return redirect()->route('admin::devices')->with('error', 'Неизвестное устройство');
        }

        $places = Place::all();
        $possibleTemps = config('devices.possible_temp');
        $cellsVariants = config('devices.cells_count');

        return view('admin.devices.edit', compact('device', 'possibleTemps', 'cellsVariants', 'places'));
    }

    public function update($id, StoreDeviceRequest $request)
    {
        $device = Device::find($id);

        if(!$device) {
            return redirect()->route('admin::devices')->with('error', 'Неизвестное устройство');
        }

        $device->update($request->all());

        return redirect()->route('admin::devices')->with('success', 'Обновлено');
    }

    public function destroy($id)
    {
        $device = Device::find($id);

        if(!$device) {
            return redirect()->route('admin::devices')->with('error', 'Неизвестное устройство');
        }

        $inRent = false;
        foreach ($device->cells as $cell) {
            if($cell->status === Cell::IN_USE) {
                $inRent = true;
                break;
            }
        }

        if($inRent) {
            return redirect()->route('admin::devices')->with('error', 'Недопустимая операция. Устройство используется');
        }

        $cellIds = [];
        foreach ($device->cells as $cell) {
            $cellIds[] = $cell->id;
        }

        Rent::whereIn('cell_id', $cellIds)->delete();
        Cell::where('device_id', '=', $device->id)->delete();

        $device->delete();

        return redirect()->route('admin::devices')->with('success', 'Удалено');
    }

    public function cells($deviceId, DeviceService $deviceService)
    {
        $device = Device::find($deviceId);
        $cells = $device->cells;
        if( !$device ) {
            return redirect()->route('admin::devices')->with('error', 'Неизвестное устройство');
        }

        $response = $deviceService->request($device->ip_address);
        if($response == '') {
            request()->session()->flash('error', 'Ошибка при обращении к устройству. Статусы ячеек не обновились');
            return view('admin.devices.cells', compact('device', 'cells'));
        }

        if($device->alert == 1) {
            $device->alert = 0;
            $device->save();
        }

        $cellsStatuses = json_decode($response, true);

        $deviceService->updateDoorsStatus($cells, $cellsStatuses);

        return view('admin.devices.cells', compact('device', 'cells'));
    }

    public function getTemperatures($id, $period)
    {
        $from = $this->getPeriod($period);
        $temps = DeviceTemperature::where('device_id', $id)->where('created_at', '>=', $from)->get();
        if(in_array($period, ['1w', '30d'])) {
            $temps = $this->severalDaysData($temps, $period);
        }

        return response()->json(DeviceTemperatureCollection::make($temps));
    }

    // TODO Refactor all below
    private function getPeriod($period)
    {
        switch ($period) {
            case '15min':
                $from = Carbon::now()->subMinutes(15);
                break;
            case '1h':
                $from = Carbon::now()->subHour();
                break;
            case '4h':
                $from = Carbon::now()->subHours(4);
                break;
            case '1d':
                $from = Carbon::now()->subDay();
                break;
            case '1w':
                $from = Carbon::now()->subWeek();
                break;
            case '30d':
                $from = Carbon::now()->subDays(30);
                break;
            default:
                $from = Carbon::now()->subDay();
        }

        return $from;
    }

    private function severalDaysData($temps, $period)
    {
        $temps = $period === '1w'
            ? $this->getWeekData($temps)
            : $this->getMonthData($temps);

        return $temps;
    }

    private function getWeekData(Collection $temps): Collection
    {
        $data = [];
        $temps = $temps->groupBy(function($item) {
            return date('Y-m-d H', strtotime($item->created_at));
        });

        $temps->each(function ($item, $key) use (&$data) {
            $tempSum = $item->sum('temp');
            $deviceTemperature = new DeviceTemperature();
            $deviceTemperature->temp = number_format($tempSum / $item->count(), 2, '.', ' ');
            $deviceTemperature->created_at = $key . ":00";
            $data[] = $deviceTemperature;
        });

        return collect($data);
    }

    private function getMonthData(Collection $temps): Collection
    {
        $data = [];
        $temps = $temps->groupBy(function($item) {
            return date('Y-m-d', strtotime($item->created_at));
        });

        $temps->each(function ($item, $key) use (&$data) {
            $tempSum = $item->sum('temp');
            $deviceTemperature = new DeviceTemperature();
            $deviceTemperature->temp = number_format($tempSum / $item->count(), 2, '.', ' ');
            $deviceTemperature->created_at = $key;
            $data[] = $deviceTemperature;
        });

        return collect($data);
    }

    public function downloadTemperatures($id, Request $request, Excel $excel)
    {
        $device = Device::findOrFail($id);
        $from = $request->get('start_date')
            ? Carbon::createFromFormat('Y-m-d H:i', $request->get('start_date'))
            : Carbon::now()->subDays(2)->startOfDay();
        $to = $request->get('end_date')
            ? Carbon::createFromFormat('Y-m-d H:i', $request->get('end_date'))
            : Carbon::now()->subDays(2)->endOfDay();


        return $excel->download(new DeviceTemperatures($device->id, $from, $to),$device->place->address . ' temperatures ' . $from->format('Y-m-d H:i') . '-' . $to->format('Y-m-d H:i') . '.xls');
    }
}
