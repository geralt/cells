<?php

namespace App\Http\Controllers\Admin;

use App\DTO\User\UserFilterData;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\StoreUserRequest;
use App\Http\Requests\Admin\Users\UpdateUserRequest;
use App\Models\Place;
use App\Models\Rent;
use App\Models\User;
use App\Models\UserPlace;
use App\Services\FCMService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $userFilterObject = UserFilterData::fromRequest($request);
        $users = User::getUsersList($userFilterObject);

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $places = Place::get();
        return view('admin.users.create', compact('places'));
    }

    public function store(StoreUserRequest $request)
    {
        $data = $request->all();

        $data['role'] = $request->get('role', 'user');
        $user = User::create($data);
        foreach ($data['place_ids'] as $place_id) {
            UserPlace::create([
                'user_id' => $user->id,
                'place_id' => $place_id
            ]);
        }
        return redirect()->route('admin::users')->with('success', 'Пользователь добавлен');
    }

    public function edit($id)
    {
        $user = User::find($id);
        if(!$user) {
            return redirect()->route('admin::users')->with('error', 'Неизвестный клиент');
        }

        $places = Place::get();

        return view('admin.users.edit', compact('user', 'places'));
    }

    public function update($id, UpdateUserRequest $request, FCMService $fcmService)
    {
        $user = User::find($id);

        if(!$user) {
            return redirect()->route('admin::users')->with('error', 'Неизвестный клиент');
        }

        $data = $request->all();

        if(strlen($data['password']) === 0) {
            unset($data['password']);
        }

        if($request->has('verify') && !$user->phone_verified_at) {
            $data['phone_verified_at'] = Carbon::now();
        }

        $user->update($data);

        UserPlace::where('user_id', $user->id)->delete();
        foreach ($data['place_ids'] as $place_id) {
            UserPlace::create([
                'user_id' => $user->id,
                'place_id' => $place_id
            ]);
        }

        return redirect()->route('admin::users')->with('success', 'Обновлено');
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if(!$user) {
            return redirect()->route('admin::users')->with('error', 'Неизвестный клиент');
        }

        $activeRentsCount = Rent::where('user_id', '=', $user->id)->where('is_active', '=', 1)->count();
        if($activeRentsCount > 0) {
            return redirect()->route('admin::users')->with('error', 'Недопустимая операция! У пользователя есть активная аренда');
        }

        $user->delete();

        return redirect()->route('admin::users')->with('success', 'Удалено');
    }

    public function available(Request $request)
    {
        $q = $request->get('q');

        $users = User::with(['place'])
            ->where('role', 'user')
            ->when($q, function ($query) use($q) {
                $query->where('first_name', 'LIKE', $q.'%')
                    ->orWHere('last_name', 'LIKE', $q.'%');
            })
            ->get();
        return response()->json($users);
    }
}
