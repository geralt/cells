<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Size\CreateSizeRequest;
use App\Models\Cell;
use App\Models\Device;
use App\Models\Size;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CellSizesController extends Controller
{
    public function index()
    {
        $sizes = Size::paginate(50);

        return view('admin.sizes.index', compact('sizes'));
    }

    public function showCreate()
    {
        return view('admin.sizes.create');
    }

    public function store(CreateSizeRequest $request)
    {
        Size::create([
            'alias' => $request->get('alias'),
            'width' => (int)$request->get('width'),
            'height' => (int)$request->get('height'),
            'depth' => (int)$request->get('depth'),
            'created_at' => Carbon::now()
        ]);

        return redirect()->route('admin::cells.sizes')->with('success', 'Сохранено');
    }

    public function edit($id)
    {
        $size = Size::findOrFail($id);

        return view('admin.sizes.edit', compact('size'));
    }

    public function update($id, CreateSizeRequest $request)
    {
        $size = Size::findOrFail($id);

        $size->update([
            'alias' => $request->get('alias'),
            'width' => (int)$request->get('width'),
            'height' => (int)$request->get('height'),
            'depth' => (int)$request->get('depth'),
        ]);

        return redirect()->route('admin::cells.sizes')->with('success', 'Обновлено');
    }

    public function destroy($id)
    {
        $size = Size::find($id);

        if(!$size) {
            return redirect()->route('admin::cells.sizes')->with('error', 'Неизвестный размер');
        }

        $size->delete();

        return redirect()->route('admin::cells.sizes')->with('success', 'Удалено');
    }

    public function showForm($deviceId)
    {
        $device = Device::find($deviceId);
        if(!$device) {
            return redirect()->route('admin::devices')->with('error', 'Неизвестное устройство');
        }

        $possibleSizes = Size::all();
        $cells = $device->cells;

        return view('admin.cell.sizes', compact('possibleSizes', 'cells', 'device'));
    }

    public function saveSizes($deviceId, Request $request)
    {
        $device = Device::find($deviceId);
        if(!$device) {
            return redirect()->route('admin::devices')->with('error', 'Неизвестное устройство');
        }

        $sizes = $request->get('size', []);

        (new Cell)->sizeMassUpdate($sizes);

        return redirect()->route('admin::devices')->with('success', 'Размеры ячеек сохранены');
    }
}
