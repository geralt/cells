<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\Auth\LoginRequest;
use App\Services\AuthService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function showLogin()
    {
        return view('admin/auth');
    }

    public function login(LoginRequest $request)
    {
        if ($this->authService->login($request->all()))
        {
            $user = Auth::user();
            $remember = $request->has('remember');

            if($user->role != 'admin' && $user->role != 'superadmin') {
                $errors = new MessageBag();
                $errors->add('phone', 'Нет прав');
                return redirect()->to('/admin/login')->withErrors($errors);
            }

            Auth::login($user, $remember);
            return redirect()->intended('/');
        }

        $errors = new MessageBag();
        $errors->add('password', 'Неправильные логин или пароль');

        return redirect()->to('/admin/login')->withErrors($errors);
    }

    public function logout()
    {
        $this->authService->logout();
        return redirect('/admin/login');
    }
}
