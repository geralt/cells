<?php

namespace App\Http\Controllers;

use App\Models\Page;

class PagesController extends Controller
{
    public function show($slug)
    {
        $page = Page::where('slug', $slug)->first();
        abort_if(is_null($page), 404);
        return response()->file(storage_path('app/public/' . $page->filename));
    }
}
