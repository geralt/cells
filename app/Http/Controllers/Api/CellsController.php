<?php

namespace App\Http\Controllers\Api;

use App\Models\Cell;
use App\Models\Device;
use App\Models\ExtendRent;
use App\Models\Size;
use App\Services\CellService;
use App\Services\DeviceService;
use App\Services\FCMService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mockery\Exception;

class CellsController extends ApiController
{
    private $cellService;

    public function __construct(CellService $cellService)
    {
        parent::__construct();
        $this->cellService = $cellService;
    }

    public function getList(Request $request)
    {
        try{
            $user = $request->user;
            /*$devices = $user->getDevicesWithCells();
            $result = $this->cellService->groupAllowedCells($devices);*/
            $result = $this->cellService->getSellsByAddresses($user);
            /*
             * May be this format will be need in future
             * foreach($devices as $device) {
                $device->setHidden(['created_at', 'updated_at', 'place_id', 'place']);
                $device->address = $device->place->address;
                foreach ($device->cells as $cell) {
                    $cell->setHidden(['created_at', 'updated_at', 'opens_total', 'opens_by_rent']);
                }
            }*/

            if($result === []) {
                $result = [
                    "addresses" => []
                ];
            }

            return $this->response->success($result, 'success');
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Неожиданная ошибка');
        }
    }

    public function myCells(Request $request)
    {
        try{
            $user = $request->user;
            $devices = $user->getDevicesWithCells();
            $result = $this->cellService->getRentCells($user, $devices);

            return $this->response->success(['cells' => $result, 'has_address' => $user->places->count() > 0], 'success');
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Неожиданная ошибка');
        }
    }

    public function rentCell(Request $request)
    {
        $zone = strtolower((string)$request->get('zone'));
        $since = $request->get('since');
        $hours = (int)$request->get('hours', 0);
        $alias = $request->get('dimension_type');
        $placeId = (int)$request->get('place_id', 0);

        $temp = Device::getTemp($zone);

        if(is_null($temp) || is_null($since)) {
            return $this->response->failure('Не указана температура или дата начала аренды');
        }

        if($hours < 1) {
            return $this->response->failure('Нельзя арендовать ячейку меньше, чем на час');
        }

        try {
            if($since == 'now') {
                $sinceDate = Carbon::now();
            }else {
                $sinceDate = Carbon::createFromFormat('Y-m-d H:i', $since);
            }
            $now = Carbon::now();

            if($now->subHour()->gte($sinceDate)) {
                return $this->response->failure('Дата начала аренды не может быть раньше, чем текущая дата');
            }

            if($sinceDate->diffInHours(Carbon::now()) > 49 ) {
                return  $this->response->failure('Дата начала аренды не может быть позже, чем черезз 48 часов');
            }

            $now->addHour(); // add previously changed value
            $user = $request->user;

            $sizeIds = Size::where('alias', $alias)->get()->pluck('id');

            $result = $this->cellService->rentCell( $user, $temp, $sinceDate, $hours, $sizeIds, $placeId);

            if(!is_null($result)) {
                return $this->response->success($result, 'Successful rent due ' . $result['due']);
            }

            return $this->response->failure('Нет свободной ячейки');
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Неожиданная ошибка');
        }
    }

    public function extendRent($id,Request $request)
    {
        $cellId = (int)$id;
        $hours = (int)$request->get('hours');

        try {
            $cell = Cell::with('current_rent')->find($cellId);
            if(!$cell) {
                return $this->response->failure('Неправильная ячейка');
            }

            $rent = $cell->current_rent;

            if($rent->user_id != $request->user->id || $rent->end_date < Carbon::now()) {
                return $this->response->failure("У вас нет прав на это!");
            }

            ExtendRent::create([
                'rent_id' => $rent->id,
                'extend_from' => $rent->end_date,
                'hours' => $hours
            ]);

            $rent->end_date = $rent->end_date->addHours($hours);
            $rent->save();

            return $this->response->success(['cell' => ['id' => $cellId, 'rent_due' => $rent->end_date->format('Y-m-d H:i')]], 'Rent period updated');
        } catch (Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Неизвестная ошибка');
        }

    }

    public function openDoor($id, Request $request, DeviceService $deviceService, FCMService $fcmService)
    {
        $cell = Cell::find($id);
        if(is_null($cell)) {
            return $this->response->failure('Неправильная ячейка');
        }

        $rent = $cell->current_rent;
        if(is_null($rent) || $rent->user_id != $request->user->id || $rent->is_active == 0) {
            return $this->response->failure('У вас нет прав на совершение этих действий');
        }

        try {
            $device = $cell->device;
            $deviceService->retrieveDoorsStatuses($device);

            if($cell->is_door_open == 1) {
                return $this->response->failure("Дверь уже открыта");
            }

            $isOkOn = $deviceService->sendOpenCloseRequest($cell, 'on');
            if(!$isOkOn) {
                return $this->response->failure('Ошибка при обращении к устройству');
            }

            sleep(1);
            $isOkOff = $deviceService->sendOpenCloseRequest($cell, 'off');
            if(!$isOkOff) {
                return $this->response->failure('Ошибка при обращении к устройству');
            }

            $cell->opens_total++;
            $cell->save();

            $rent->opens_count++;
            $rent->save();

            $deviceService->retrieveDoorsStatuses($device);

            if($request->user->device_id) {
                $fcmService->send($request->user->device_id, 'Дверь открыта', 'Адрес устройства: ' . $device->place->address . ', номер ячейки: '. $cell->number);
            }

            return $this->response->success(["status" => "success"], 'Дверь открыта');
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Неожиданная ошибка');
        }
    }

    public function cancelRent($id, Request $request)
    {
        try {
            $cell = Cell::find($id);
            if (is_null($cell)) {
                return $this->response->failure('Неправильная ячейка');
            }

            $user = $request->user;

            $rent = $cell->current_rent;
            if (is_null($rent) || $rent->user_id != $user->id) {
                return $this->response->failure('У вас нет прав на совершение этих действий');
            }

            $rent->cancelled_at = Carbon::now();
            $rent->is_active = 0;
            $rent->save();

            $cell = $rent->cell;
            $cell->status = Cell::FREE;
            $cell->rent_id = NULL;
            $cell->save();

            return $this->response->success(['status' => 'success'], 'Rent cancelled at ' . $rent->cancelled_at->format('Y-m-d H:i'));
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Неожиданная ошибка');
        }
    }

    public function getDoorStatus($cellId, Request $request, DeviceService $deviceService, FCMService $fcmService)
    {
        $cell = Cell::with('current_rent')->find($cellId);
        if(is_null($cell)) {
            return $this->response->failure('Неправильная ячейка');
        }

        if(!$cell->current_rent || $request->user->id !== $cell->current_rent->user_id) {
            return $this->response->failure('У вас нет прав на совершение этих действий');
        }

        $currentStatus = $cell->is_door_open;

        $device = $cell->device;
        $deviceService->retrieveDoorsStatuses($device);

        if((bool)$currentStatus === true && (bool)$cell->is_door_open === false) {
            $fcmService->send($request->user->device_id, 'Дверь закрыта', 'Адрес устройства: ' . $device->place->address . ', номер ячейки: '. $cell->number);
        }

        return $this->response->success(['is_door_open' => (bool)$cell->is_door_open], 'ok');
    }

    public function generateAccessToken(Request $request, $cellId)
    {
        $cell = Cell::find($cellId);

        if(!$cell) {
            return $this->response->failure('Неизвестная ячейка');
        }

        if($cell->current_rent->user_id != $request->user->id) {
            return $this->response->failure('У вас нет прав доступа к ячейке');
        }

        $cell->access_token = Str::random(40);
        $cell->access_token_expires = Carbon::now()->addMinutes(20);

        $cell->save();

        return $this->response->success([
            'token' => $cell->access_token,
            'url' => "/cell/$cell->access_token"
            ], 'ok');
    }
}
