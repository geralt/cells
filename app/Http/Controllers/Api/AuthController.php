<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\API\Auth\ChangePasswordRequest;
use App\Http\Requests\API\Auth\CheckCodeRequest;
use App\Http\Requests\API\Auth\CreateUserRequest;
use App\Http\Requests\API\Auth\LoginRequest;
use App\Http\Requests\API\Auth\RecoveryRequest;
use App\Http\Requests\API\Auth\ResendCodeRequest;
use App\Models\User;
use App\Services\AuthService;
use App\Services\FCMService;
use App\Services\TwilioService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthController extends ApiController
{
    private $authService;
    private $twilio;

    public function __construct(AuthService $authService, TwilioService $twilioService)
    {
        parent::__construct();
        $this->authService = $authService;
        $this->twilio = $twilioService;
    }

    public function register(CreateUserRequest $request)
    {
        try {
            $user = User::where('phone', '=', $request->phone)->first();
            if(!$user) {
                $user = $this->authService->register($request->all());
                $this->twilio->sendCode($request->get('phone'));

                return $this->response->success([
                    'id' => $user->id,
                    'token' => $user->token,
                    'need_verification' => true,
                    'has_address' => $user->place_id > 0
                ], 'Зарегистрировано');
            } else {
                if($user->phone_verified_at) {
                    $token = $this->authService->login($request->all());
                    if ($token) {
                        return $this->response->success([
                            'token' => $token,
                            'need_verification' => false,
                            'has_address' => $user->place_id > 0
                            ], 'Успешный вход');
                    }

                    return $this->response->failure('Неправильные учетные данные', 405);
                } else {
                    $token = $this->authService->login($request->all());
                    $this->twilio->resendCode($request->get('phone'));

                    if ($token) {
                        return $this->response->success([
                            'token' => $token,
                            'need_verification' => true,
                            'has_address' => $user->place_id > 0
                            ], 'Успешный вход');
                    }

                    return $this->response->failure('Неправильные учетные данные', 405);
                }
            }


        } catch (\Exception $e) {
            Log::error($e->getMessage().' '.$e->getTraceAsString());
            return $this->response->failure('Телефон, пароль, имя и фамилия обязательны для заполнения, пользователей с таким же номером телефона не должно быть в системе');
        }
    }

    public function login(LoginRequest $request)
    {
        try {
            $token = $this->authService->login($request->all());
            $needVerification = empty(Auth::user()->phone_verified_at);

            if($token && $needVerification) {
                $this->twilio->resendCode($request->phone);
            }

            if ($token) {
                return $this->response->success([
                    'token' => $token,
                    'need_verification' => $needVerification,
                    'has_address' => Auth::user() && Auth::user()->place_id > 0
                    ], 'Успешный вход');
            }

            return $this->response->failure('Неправильные учетные данные', 405);
        } catch (\Exception $e) {
            return $this->response->failure('Неправильные учетные данные', 405);
        }
    }

    public function checkCode(CheckCodeRequest $request)
    {
        try {
            $status = $this->twilio->checkCode($request->all());

            if($status !== 'approved') {
                $this->response->failure('При подтверждении кода возникла ошибка');
            }

            return $this->response->success(['status' => "success"], '');
        } catch (\Exception $e) {
            return $this->response->failure('Ошибка', 400);
        }
    }

    public function resendCode(ResendCodeRequest $request)
    {
        try {
            $this->twilio->resendCode($request->get('phone'));
            return  $this->response->success(['status' => "success"], 'Новый код отправлен');
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Ошибка', 400);
        }
    }

    public function getRecoveryCode(RecoveryRequest $request, TwilioService $twilioService)
    {
        $user = User::where('phone', '=', $request->get('phone'))->first();
        if(!$user) {
            return $this->response->failure("Не найден", 404);
        }

        try {
            $user->generateRecoveryHash();

            $twilioService->sendSMS($user->phone, "Ваш код для смены пароля: " . $user->recovery_code . ". Пожалуйста, никому не сообщайте его.");

            return $this->response->success(['status' => 'success'], 'ok');
        } catch(\Exception $e) {
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $this->response->failure('Ошибка', 400);
        }
    }

    public function changePassword(ChangePasswordRequest $request, AuthService $authService, FCMService $fcmService)
    {
        $user = User::where('phone', '=', $request->get('phone'))->first();
        if(!$user) {
            return $this->response->failure("Не найден", 404);
        }

        $code = $request->get('code');
        if($user->recvery_active_due >= Carbon::now()) {
            return $this->response->failure("Код больше недействителен. Запросите новый");
        }

        if($code != $user->recovery_code) {
            return $this->response->failure('Неверный код');
        }

        $password = $request->get('password');
        $authService->changePass($user, $password);

        if($user->device_id) {
            $fcmService->send($user->device_id, 'Пароль успешно изменен', '');
        }

        $token = $this->authService->login(['phone' => $user->phone, 'password' => $password]);
        $needVerification = empty($user->phone_verified_at);

        return $this->response->success([
            'token' => $token,
            'need_verification' => $needVerification,
            'has_place' => $user->place_id > 0], 'logged in');

    }
}
