<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\DeviceResource;
use Illuminate\Http\Request;

class DevicesController extends ApiController
{
    public function getList(Request $request)
    {
        $user = $request->user;

        $devices = $user->getDevicesWithCells();

        return $this->response->success(collect(DeviceResource::collection($devices))->toArray(), 'ok');
    }
}
