<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CellSizes\CellSizeCollection;
use App\Models\Cell;
use App\Models\Size;
use Illuminate\Http\Request;

class UserController extends ApiController
{
    public function setFcmToken(Request $request)
    {
        $deviceId = $request->get('device_id');
        if(!$deviceId) {
            return $this->response->failure('Не указано обязательное поле device_id');
        }

        $user = $request->user;
        $user->device_id = $deviceId;
        $user->save();

        return $this->response->success(['success' => true], 'ok');
    }

    public function getPossibleSizes(Request $request)
    {
        $devicesIds = $request->user->getDevicesWithCells()->pluck('id');
        $sizesIds = Cell::whereIn('device_id', $devicesIds)->get()->pluck('size_id');
        $sizes = Size::whereIn('id', $sizesIds)->get();

        return $this->response->success(collect(CellSizeCollection::make($sizes))->toArray(), 'ok');
    }
}
