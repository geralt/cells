<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\ApiResponse;

class ApiController extends Controller
{
    protected $response;

    public function __construct()
    {
        $this->response = new ApiResponse();
    }
}
