<?php

namespace App\Http\Controllers\Api;

use App\Models\Reference;
use App\Models\UserPlace;
use Illuminate\Http\Request;

class ReferenceController extends ApiController
{
    public function getReference(Request $request)
    {
        $userPlace = UserPlace::where('user_id', $request->user->id)->first();
        $reference = Reference::when($userPlace,
            function($q) use ($userPlace) {
                $q->where('place_id', '=', $userPlace->place_id);
            },
            function($q) {
                $q->where('place_id', '=', 0);
            })
            ->first();
        if(!$reference) {
            return $this->response->failure('Не задано');
        }

        return $this->response->success(['text' => $reference->text], 'ok');
    }
}
