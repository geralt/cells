<?php

namespace App\Http\Controllers;

use App\Models\Cell;
use App\Services\DeviceService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CellController extends Controller
{
    public function showOpenDialog($token)
    {
        $cell = Cell::with(['device'])->getByToken($token);

        if(!$cell) {
            abort(404);
        }

        return view('cells.show_open_cell', compact('cell', 'token'));
    }

    public function open($token, DeviceService $deviceService)
    {
        $cell = Cell::with(['device'])->getByToken($token);

        if(!$cell) {
            abort(404);
        }

        $isDoorOpened = $deviceService->openDoor($cell, true);

        if($isDoorOpened) {
            Log::info('Door opened for cell : '.$cell->device->mac . '/'.$cell->number.' by token '. $token .' at ' . Carbon::now()->format('Y-m-d H:i:s'));
        }

        return view('cells.opened', compact('cell', 'isDoorOpened'));
    }
}
