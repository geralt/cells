<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceTemperature extends Model
{
    protected $fillable = [
        'device_id', 'temp', 'target_temp', 'created_at'
    ];

    public $timestamps = false;
    protected $dates = ['created_at'];

    protected $casts = [
        'created_at'  => 'datetime:Y-m-d H:i',
    ];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
