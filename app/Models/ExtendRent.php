<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtendRent extends Model
{
    protected $fillable = [
        'rent_id', 'extend_from', 'hours'
    ];

    protected $dates = ['created_at', 'updated_at', 'extend_from'];

    protected $casts = [
        'extend_from' => 'datetime',
    ];
}
