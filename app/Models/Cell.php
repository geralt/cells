<?php

namespace App\Models;

use App\QueryBuilders\CellsQueryBuilder;
use Illuminate\Database\Eloquent\Model;

class Cell extends Model
{
    const FREE = 0;
    const IN_USE = 1;
    const ON_SERVICE = 2;

    protected $fillable = [
        'number', 'device_id', 'current_temp', 'is_door_open', 'status', 'rent_id', 'opens_by_rent', 'opens_total', 'size_id',
        'access_token', 'access_token_expires'
    ];

    protected $dates = ['created_at', 'updated_at', 'access_token_expires'];

    public function newEloquentBuilder($query)
    {
        return new CellsQueryBuilder($query);
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    public function rents()
    {
        return $this->hasMany(Rent::class);
    }

    public function current_rent()
    {
        return $this->hasOne(Rent::class, 'id', 'rent_id');
    }

    public function active_rent()
    {
        return $this->hasMany(Rent::class)->with('user')->where('is_active', '=', 1)->first();
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }
}
