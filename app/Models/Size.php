<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = [
        'width', 'height', 'depth', 'created_at', 'alias'
    ];

    public $timestamps = false;

    protected $dates = ['created_at'];

    public function getLabelAttribute($value)
    {
        return "{$this->width}x{$this->height}x{$this->depth}";
    }
}
