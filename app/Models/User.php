<?php

namespace App\Models;

use App\QueryBuilders\UserQueryBuilder;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    const ADMIN = 1;
    const USER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'phone', 'last_name', 'token', 'place_id', 'last_activity',
        'patronymic', 'phone_verified_at', 'device_id', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone_verified_at' => 'datetime',
        'recovery_active_due' => 'datetime'
    ];

    protected $dates = ['created_at', 'updated_at', 'token_expires', 'recovery_active_due'];

    public function newEloquentBuilder($query)
    {
        return new UserQueryBuilder($query);
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setPhoneAttribute($value) {
        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->last_name) . ' ' . ucfirst($this->first_name);
    }

    public function places()
    {
        return $this->hasManyThrough(
            Place::class,
            UserPlace::class,
            'user_id',
            'id',
            'id',
            'place_id'
        );
    }

    public function user_places()
    {
        return $this->hasMany(UserPlace::class);
    }

    public function rent()
    {
        return $this->hasMany(Rent::class);
    }

    public function phoneAddress()
    {
        return $this->hasOne(PhoneAddress::class, 'phone', 'phone');
    }

    public function getDevicesWithCells($temp = null)
    {
        $places_ids = $this->user_places->pluck('place_id')->toArray();
        return Device::with(['cells', 'place'])
            ->whereIn('place_id', $places_ids)
            ->when($temp, function ($q) use ($temp) {
                $q->where('target_temp', $temp);
            })
            ->get();
    }

    public function getDevicesWithCellsBuilder($temp = null)
    {
        $places_ids = $this->user_places->pluck('place_id')->toArray();
        return Device::with(['cells', 'place'])
            ->whereIn('place_id', $places_ids)
            ->when($temp, function ($q) use ($temp) {
                $q->where('target_temp', $temp);
            });
    }

    public function generateRecoveryHash()
    {
        $this->recovery_code = random_int(100000, 999999);
        $this->recovery_active_due = Carbon::now()->addMinutes(30);
        $this->save();
    }

    public function generateToken()
    {
        $this->token = Str::random(40);
        $this->token_expires = Carbon::now()->addDays(2);
        $this->save();
    }

    public function updateToken()
    {
        $this->token_expires = Carbon::now()->addDays(2);
        $this->save();
    }

}
