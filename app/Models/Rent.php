<?php

namespace App\Models;

use App\QueryBuilders\RentQueryBuilder;
use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $fillable = [
        'user_id', 'cell_id', 'start_date', 'end_date', 'is_active', 'cancelled_at'
    ];

    protected $dates = ['created_at', 'updated_at', 'start_date', 'end_date', 'cancelled_at'];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'cancelled_at' => 'datetime'
    ];

    public function newEloquentBuilder($query)
    {
        return new RentQueryBuilder($query);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cell()
    {
        return $this->belongsTo(Cell::class);
    }

    public function extendRents()
    {
        return $this->hasMany(ExtendRent::class);
    }
}
