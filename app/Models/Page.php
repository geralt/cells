<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'slug', 'filename'
    ];

    protected $dates = ['created_at', 'updated_at'];
}
