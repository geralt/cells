<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $fillable = [
        'place_id', 'text'
    ];

    protected $primaryKey = 'place_id';
    public $timestamps = false;

    public function place()
    {
        return $this->belongsTo(Place::class);
    }
}
