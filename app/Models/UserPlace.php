<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPlace extends Model
{
    protected $fillable = [
        'user_id', 'place_id'
    ];

    public $timestamps = false;
}
