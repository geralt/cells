<?php

namespace App\Models;

use App\QueryBuilders\PhoneAddressQueryBuilder;
use Illuminate\Database\Eloquent\Model;

class PhoneAddress extends Model
{
    protected $table = 'phone_address';
    protected $primaryKey = 'phone';

    protected $fillable = [
        'phone', 'place_id'
    ];

    public $timestamps = false;

    public function newEloquentBuilder($query)
    {
        return new PhoneAddressQueryBuilder($query);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function setPhoneAttribute($value) {
        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }
}
