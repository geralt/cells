<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Device extends Model
{
    const HEAT = 40;
    const NEUTRAL = 20;
    const FRIDGE = 4;
    const FREEZE = -18;

    const LABELS = [
        self::HEAT => 'heat',
        self::NEUTRAL => 'neutral',
        self::FRIDGE => 'fridge',
        self::FREEZE => 'freeze'
    ];

    const LABELS_REVERSE = [
        'heat' => self::HEAT,
        'neutral' => self::NEUTRAL,
        'fridge' => self::FRIDGE,
        'freeze' => self::FREEZE
    ];

    protected $fillable = [
        'mac', 'place_id', 'current_temp', 'target_temp', 'cells_count', 'ip_address', 'token'
    ];

    protected $dates = ['created_at', 'updated_at'];

    protected static function booted()
    {
        static::creating(function ($device) {
            $device->token = Str::random(40);
        });
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function cells()
    {
        return $this->hasMany(Cell::class);
    }

    public function temperatures()
    {
        return $this->hasMany(DeviceTemperature::class);
    }

    public function getLabel()
    {
        return self::LABELS[$this->target_temp];
    }

    public static function getTemp($zoneName)
    {
        return array_key_exists($zoneName, self::LABELS_REVERSE) ? self::LABELS_REVERSE[$zoneName] : null;
    }

    public function getLastTemp()
    {
        return $this->temperatures()->orderBy('created_at', 'desc')->first();
    }
}
