<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = [
        'title', 'address', 'company', 'responsible_person', 'phone', 'comment', 'reference', 'latitude', 'longitude'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'deleted_at' => 'datetime'
    ];

    public function setPhoneAttribute($value) {
        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }
}
