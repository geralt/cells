<?php

namespace App\Providers;

use App\Services\Table\Table;
use Illuminate\Support\ServiceProvider;

class TableServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Services\Table\Table', function ($app) {
            return new Table($app);
        });

        $this->app->alias('App\Services\Table\Table', 'service.table');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Services\Table\Table', 'service.table'];
    }
}
