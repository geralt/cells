<?php

namespace App\QueryBuilders;

use App\Services\Table\Sorter;
use Illuminate\Database\Eloquent\Builder;

class RentQueryBuilder extends Builder
{
    public function buildReport(Sorter $sorter, $phone, $dateStart)
    {
        return $this->leftJoin('cells', 'rents.cell_id', '=', 'cells.id')
            ->leftJoin('users', 'rents.user_id', '=', 'users.id')
            ->selectRaw("ABS(TIMESTAMPDIFF(HOUR, rents.end_date, rents.start_date)) as diff, rents.id, start_date, end_date, user_id, cell_id, opens_count, users.phone, cells.device_id")
            ->when($phone, function ($query) use ($phone) {
                $query->whereHas('user', function ($q) use ($phone) {
                    $q->where('phone', '=', $phone);
                });
            })
            ->when($dateStart, function ($query) use ($dateStart) {
                $query->where('start_date', '>=', $dateStart);
            })
            ->when($sorter->column() == 'name', function ($q) use ($sorter) {
                $q->orderBy('last_name', $sorter->direction());
                $q->orderBy('first_name', $sorter->direction());
            })
            ->when(in_array($sorter->column(), ['start_date', 'end_date', 'opens_count']), function ($q) use ($sorter) {
                $q->orderBy($sorter->column(), $sorter->direction());
            })
            ->when($sorter->column() == 'hours', function ($q) use ($sorter) {
                $q->orderBy('diff', $sorter->direction());
            });
    }
}
