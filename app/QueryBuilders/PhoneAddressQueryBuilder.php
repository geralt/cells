<?php

namespace App\QueryBuilders;

use App\Services\Table\Sorter;
use Illuminate\Database\Eloquent\Builder;

class PhoneAddressQueryBuilder extends Builder
{
    public function getAddresses(Sorter $sorter)
    {
        return $this->join('places', 'phone_address.place_id', '=', 'places.id')
            ->select(['phone_address.place_id', 'phone_address.phone', 'places.address'])
            ->when($sorter->column() == 'phone', function ($q) use($sorter) {
                $q->orderBy('phone_address.phone', $sorter->direction());
            })
            ->when($sorter->column() == 'address', function ($q) use($sorter) {
                $q->orderBy('places.address', $sorter->direction());
            })
            ->paginate(50);
    }
}
