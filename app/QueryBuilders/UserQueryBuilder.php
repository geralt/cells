<?php

namespace App\QueryBuilders;

use App\DTO\User\UserFilterData;
use App\Services\Table\Sorter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class UserQueryBuilder extends Builder
{
    const LIST_PAGINATION_ITEMS_COUNT = 50;

    public function getUsersList(UserFilterData $userData)
    {
        return $this->with(['places'])
            ->when(Auth::user()->role !== 'superadmin',
                function ($q) {
                    $q->where('role', 'user');
                },
                function ($q) {
                    $q->where('role', '<>', 'superadmin');
                }
            )
            ->when($userData->firstName, function ($query) use ($userData) {
                $query->where('first_name', $userData->firstName);
            })
            ->when($userData->lastName, function ($query) use ($userData) {
                $query->where('last_name', $userData->lastName);
            })
            ->when($userData->phone, function ($query) use ($userData) {
                $query->where('phone', $userData->phone);
            })
            ->paginate(self::LIST_PAGINATION_ITEMS_COUNT);
    }
}
