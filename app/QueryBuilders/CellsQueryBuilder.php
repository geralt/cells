<?php

namespace App\QueryBuilders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class CellsQueryBuilder extends Builder
{
    /**
     * @param array $sizes
     *
     * UPDATE cells SET size_id = CASE
     *                              WHEN id = 1 THEN 3
     *                              WHEN id = 3 THEN 5
     *                            END
     * WHERE ID IN (1, 3)
     */
    public function sizeMassUpdate(array $sizes)
    {
        $cases = [];
        $ids = [];
        $params = [];
        foreach ($sizes as $cellId => $sizeId) {
            if((int)$sizeId > 0) {
                $cases[] = "WHEN {$cellId} then ?";
                $params[] = $sizeId;
                $ids[] = $cellId;
            }
        }

        $ids = implode(',', $ids);
        $cases = implode(' ', $cases);

        DB::update("UPDATE cells SET `size_id` = CASE `id` {$cases} END WHERE `id` in ({$ids})", $params);
    }

    public function getByToken(string $token)
    {
        return $this->where('access_token', $token)
            ->where('access_token_expires', '>=', Carbon::now())
            ->first();
    }
}
