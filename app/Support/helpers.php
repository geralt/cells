<?php

use \Illuminate\Support\Facades\Auth;
use \App\Models\User\Role;

function is_admin()
{
    return Auth::user() && (Auth::user()->role == 'admin' || Auth::user()->role == 'superadmin');
}

function user()
{
    return Auth::user();
}
