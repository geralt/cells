<?php

namespace App\Exports;

use App\Models\DeviceTemperature;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;

class DeviceTemperatures implements FromCollection
{
    private $id;
    private $from;
    private $to;

    public function __construct(int $id, Carbon $from, Carbon $to)
    {
        $this->id = $id;
        $this->from = $from;
        $this->to = $to;
    }

    public function collection()
    {
        return DeviceTemperature::where('device_id', $this->id)
            ->where('created_at', '>=', $this->from)
            ->where('created_at', '<=', $this->to)
            ->select('created_at', 'temp')
            ->get();
    }
}
