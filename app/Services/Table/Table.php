<?php

namespace App\Services\Table;

use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;

class Table
{
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function sort(array $columns, $default = null)
    {
        $sorter = new Sorter($this->app['request'], $this->app['url']);

        $sorter->make($columns, $default);

        return $sorter;
    }
}
