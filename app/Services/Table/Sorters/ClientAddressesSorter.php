<?php

namespace App\Services\Table\Sorters;

use App\Services\Table\Sorter;

class ClientAddressesSorter implements ISorter
{
    public static function getColumns(): Sorter
    {
        return app('service.table')->sort([
            'phone' => 'asc',
            'address' => 'asc',
        ], 'phone');
    }
}
