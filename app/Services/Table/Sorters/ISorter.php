<?php

namespace App\Services\Table\Sorters;

use App\Services\Table\Sorter;

interface ISorter
{
    public static function getColumns():Sorter;
}
