<?php

namespace App\Services\Table\Sorters;

use App\Services\Table\Sorter;

class RentSorter implements ISorter
{
    public static function getColumns(): Sorter
    {
        return app('service.table')->sort([
            'name' => 'asc',
            'opens_count' => 'desc',
            'start_date' => 'desc',
            'end_date' => 'desc',
            'hours' => 'desc'
        ], 'end_date');
    }
}
