<?php

namespace App\Services\Table;

use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;

class Sorter
{
    protected $request;

    protected $url;

    protected $key = '1';

    protected $default;

    protected $columns;

    protected $directions;

    public function __construct(Request $request, UrlGenerator $url)
    {
        $this->request = $request;
        $this->url = $url;
    }

    public function make($columns, $default)
    {
        $this->setColumns($columns);
        $this->setDefault($default);
    }

    public function route($name, $column, $parameters = [], $absolute = true)
    {
        $parameters = is_array($parameters) ? $parameters : [$parameters];

        $parameters = array_merge($parameters, $this->request->query(), $this->query($column));

        return $this->url->route($name, $parameters, $absolute);
    }

    public function fa($column)
    {
        if ($this->isCurrent($column)) {
            $caret = $this->direction() === 'asc' ? 'caret-up' : 'caret-down';

            return ' <span class="fa fa-'.$caret.'"></span>';
        }

        return '';
    }

    public function query($column)
    {
        $q = [
            'asc' => null,
            'desc' => null,
            'sort' => $this->isDefault($column) ? null : $column,
        ];

        if ($this->isCurrent($column)) {
            $dir = $this->direction();
            $q[$this->swap($dir)] = $this->directions[$column] === $dir ? $this->key : null;
        }

        return $q;
    }

    public function column()
    {
        if (! $this->exists($this->request->sort)) {
            return $this->default;
        }

        return $this->request->sort;
    }

    public function direction()
    {
        if ($this->request->asc === $this->key) {
            return 'asc';
        }

        if ($this->request->desc === $this->key) {
            return 'desc';
        }

        return $this->directions[$this->column()];
    }

    protected function isCurrent($column)
    {
        return $column === $this->column();
    }

    protected function exists($column)
    {
        return in_array($column, $this->columns);
    }

    protected function isDefault($column)
    {
        return $this->default === $column;
    }

    protected function swap($direction)
    {
        return $direction === 'asc' ? 'desc' : 'asc';
    }

    protected function setColumns($columns)
    {
        foreach ($columns as $col => $dir) {
            $column = is_string($col) ? $col : $dir;

            $this->columns[] = $column;
            $this->directions[$column] = is_string($col) ? $dir : 'asc';
        }
    }

    protected function setDefault($default)
    {
        $this->default = is_null($default) ? $this->columns[0] : $default;
    }
}
