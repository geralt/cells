<?php

namespace App\Services;

use App\Models\Cell;
use Illuminate\Support\Facades\Log;

class DeviceService
{
    private $logger;

    public function __construct(Log $log)
    {
        $this->logger = $log;
    }

    public function request($uri, $params = '', $method = 'GET')
    {
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $uri);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);

            return $result;
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage() . "\n" . $e->getTraceAsString());
            return '';
        }
    }

    public function retrieveDoorsStatuses($device)
    {
        $response = $this->request($device->ip_address);
        if($response != '') {
            $cellsStatuses = json_decode($response, true);
            $cells = $device->cells;

            $this->updateDoorsStatus($cells, $cellsStatuses);
        }
    }

    public function updateDoorsStatus($cells, array $cellsStatuses)
    {
        foreach ($cells as $cell) {
            if($cell->is_door_open != $cellsStatuses[$cell->number - 1]['pinOut']) {
                $cell->is_door_open = $cellsStatuses[$cell->number - 1]['pinOut'];
                $cell->save();
            }
        }
    }

    public function sendOpenCloseRequest(Cell $cell, $action)
    {
        try {
            $isOk = false;
            foreach (range(1, 5) as $i) {
                $response = json_decode($this->request($cell->device->ip_address . '/' . $cell->number .'/' .$action), true);
                if(is_array($response)) {
                    $isOk = true;
                    break;
                }
                sleep(1);
            }

            return $isOk;
        } catch (\Exception $e) {
            $device = $cell->device;
            $device->alert = 1;
            $device->save();
            info($e->getMessage() . "\r\n" . $e->getTraceAsString());
            return false;
        }
    }

    public function openDoor(Cell $cell, $byCourier = false)
    {
        $statusOn = $this->sendOpenCloseRequest($cell, 'on');
        if(!$statusOn) {
            return false;
        }
        sleep(1);
        $statusOff = $this->sendOpenCloseRequest($cell, 'off');
        if(!$statusOff) {
            return false;
        }

        $cell->opens_total++;
        if($byCourier) {
            $cell->access_token = null;
            $cell->access_token_expires = null;
        }
        $cell->save();

        $rent = $cell->current_rent;
        if($rent) {
            $rent->opens_count++;
            $rent->save();
        }

        return true;
    }
}
