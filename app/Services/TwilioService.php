<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Twilio\Rest\Client;

class TwilioService
{
    const CODE_LIFETIME = 600; //10 minutes
    const LOCALE = 'ru';
    const MINIMUM_INTERVAL = 60;

    private $client;

    public function __construct()
    {
        $this->client = new Client(config('twilio.account_sid'), config('twilio.auth_token'));
    }

    public function sendCode(string $phone, $locale = 'en')
    {
        try {
            $sms = $this->client->verify->v2
                ->services(config('twilio.service_id'))
                ->verifications
                ->create("+{$phone}", "sms", ["locale" => self::LOCALE]);

            Redis::set("verify:$phone", time(), 'EX', self::CODE_LIFETIME);

            return $sms;
        } catch (\Exception $e) {
            Log::error("VerifyPhone.SendCode" . $e->getMessage() . ". Phone: {$phone}");
        }
    }

    /*
     * $data contains phone, code
     * */
    public function checkCode(array $data): string
    {
        try {
            $verification_check = $this->client->verify->v2->services(config('twilio.service_id'))
                ->verificationChecks
                ->create($data['code'],
                    ["to" => "+{$data['phone']}"]
                );

            if($verification_check->status === 'approved') {
                DB::table('users')->where('phone', $data['phone'])->update([
                    'phone_verified_at' => Carbon::now()
                ]);
            }

            return $verification_check->status;
        } catch (\Exception $e) {
            Log::error("VerifyPhone.CheckCode" . $e->getMessage() . ". Phone: {$data['phone']}");
            return 'error';
        }
    }

    public function resendCode(string $phone)
    {
        $sendTime = Redis::get("verify:$phone");

        if (time() - $sendTime >= self::MINIMUM_INTERVAL) {
            $this->updateVerification($phone, 'canceled');
            $this->sendCode($phone, self::LOCALE);
        }

    }

    public function updateVerification(string $phone, string $status)
    {
        try {
            $this->client->verify->v2->services(config('twilio.service_id'))
                ->verifications("+{$phone}}")
                ->update($status);
        } catch (\Exception $e) {
            Log::error("VerifyPhone.UpdatePreviousCode". $e->getMessage() . ". Phone: {$phone}");
        }
    }

    public function sendSMS($to, $text)
    {
        try {
            $message = $this->client->messages
                ->create("+{$to}}", // to
                    ["body" => $text, "from" => "+18647193544"]
                );
            Log::info("SMS was sent to {$to} " . json_encode($message));
            return true;
        } catch (\Exception $e) {
            Log::error("Send SMS. ". $e->getMessage() . ". Phone: {$to}");
            return false;
        }
    }
}
