<?php

namespace App\Services;

class FCMService
{
    CONST URL = 'https://fcm.googleapis.com/fcm/send';
    private $headers;

    public function __construct()
    {
        $this->headers = [
            'Content-Type:application/json',
            'Authorization:key=' . env('FCM_API_KEY')
        ];
    }

    public function send($deviceToken, $title, $body)
    {
        $data = json_encode([
            'registration_ids' => [
                $deviceToken
            ],
            //'notification' => ['body' => $body, 'title' => $title],
            'data' => ['title' => $title, 'body' => $body],
            'priority' => 10
        ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        /*if ($result === FALSE) {
            info('FCM Send Error: ' . curl_error($ch));
        }*/
        info('deviceId: ' . $deviceToken . ' title: ' . $title . ' FCM result: ' . $result);
        curl_close($ch);
    }
}
