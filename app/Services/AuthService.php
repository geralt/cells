<?php

namespace App\Services;

use App\Models\PhoneAddress;
use App\Models\User\Role;
use App\Models\User;
use App\Models\UserPlace;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function login(array $data)
    {
        if (Auth::attempt(['phone' => $data['phone'], 'password' => $data['password']])) {
            $user = Auth::user();
            $user->generateToken();
            /*temporary*/
            if(!$user->place_id) {
                $userAddress = PhoneAddress::where('phone', '=', $data['phone'])->first();
                if($userAddress) {
                    $user->place_id = $userAddress->place_id;
                    $userAddress->delete();
                    $user->save();
                }
            }
            return $user->token;
        }
        return false;
    }

    public function logout()
    {
        return Auth::logout();
    }

    public function register(array $data)
    {
        $data['role'] = 'user';
        /*temporary*/
        $user = User::create($data);
        $userAddresses = PhoneAddress::where('phone', '=', $data['phone'])->get();
        if($userAddresses) {
            foreach ($userAddresses as $userAddress) {
                UserPlace::create([
                    'place_id' => $userAddress->place_id,
                    'user_id' => $user->id
                ]);
                $userAddress->delete();
            }
        }
        $user->generateToken();

        return $user;
    }

    public function forgotPassword($phone)
    {
        $user = User::where('phone', '=', $phone)->first();
        if(!$user) {
            return false;
        }
        $user->generateRecoveryHash();
    }

    public function changePass($user, $password)
    {
        $user->password = $password;
        $user->recovery_active_due = null;
        $user->save();
    }
}
