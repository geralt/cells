<?php

namespace App\Services;

use App\Http\Resources\DeviceResource;
use App\Models\Cell;
use App\Models\Rent;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class CellService
{
    public function getSellsByAddresses(User $user)
    {
        $addresses = $user->places()->with('devices')->get();
        $result = [];
        $addresses->each(function ($place) use (&$result) {
            $zones = [];
            $countFree = 0;
            $totalCount = 0;
            $zoneName = '';
            $dimensions = [];

            $devices = $place->devices()->with('cells')->get()->groupBy('target_temp');

            foreach ($devices as $temp => $device) {
                foreach ($device as $item) {
                    $cells = $item->cells()->with('size')->get();
                    $countFree += $cells->filter(function($value, $key) {
                        return $value->status === Cell::FREE;
                    })->count();
                    $totalCount += $item->cells_count;
                    if(!$zoneName) {
                        $zoneName = $item->getLabel();
                    }

                    foreach ($cells as $cell) {
                        if($cell->size) {
                            if(!array_key_exists($cell->size->label, $dimensions)) {
                                $dimensions[$cell->size->label] = [
                                    'count' => 0,
                                    'available' => 0,
                                    'dimension' => $cell->size->label,
                                    'type' => $cell->size->alias
                                ];
                            }

                            $dimensions[$cell->size->label]['count'] += 1;
                            if($cell->status === Cell::FREE) {
                                $dimensions[$cell->size->label]['available'] += 1;
                            }
                        }
                    }

                    $zones[] = [
                        'type' => $zoneName,
                        'targetTemp' => $temp,
                        "available" => $countFree,
                        "count" => $totalCount,
                        'dimensions' => collect($dimensions)->values()
                    ];
                }
            }

            $result[] = [
                'id' => $place->id,
                'latitude' => $place->latitude,
                'longitude' => $place->longitude,
                'place' => $place->address,
                'zones' => $zones
            ];
        });

        return $result;
    }

    /**
     * deprecated
     */
    public function groupAllowedCells(Collection $devices)
    {
        $result = [];
        $devices = $devices->groupBy('target_temp');
        foreach ($devices as $temp => $device) {
            $countFree = 0;
            $totalCount = 0;
            $zoneName = '';
            $sizes = [];
            foreach ($device as $item) {
                $cells = $item->cells()->with('size')->get();
                $countFree += $cells->filter(function($value, $key) {
                    return $value->status === Cell::FREE;
                })->count();
                $totalCount += $item->cells_count;
                if(!array_key_exists('address', $result)) {
                    $result['address'] = $item->place->address;
                }
                if(!$zoneName) {
                    $zoneName = $item->getLabel();
                }
                foreach ($cells as $cell) {
                    if($cell->size) {
                        if(!array_key_exists($cell->size->label, $sizes)) {
                            $sizes[$cell->size->label] = [
                                'count' => 0,
                                'available' => 0
                            ];
                        }

                        $sizes[$cell->size->label]['count'] += 1;
                        if($cell->status === Cell::FREE) {
                            $sizes[$cell->size->label]['available'] += 1;
                        }
                    }
                }
            }
            $result['cells'][] = [
                'zone' => $zoneName,
                'targetTemp' => $temp > 0 ? '+' . $temp : $temp,
                'amount' => $totalCount,
                'available' => $countFree,
                'sizes' => $sizes,
                'device' => DeviceResource::make($device->first())
            ];
        }

        return $result;
    }

    public function getRentCells($user, Collection $devices)
    {
        $result = [];

        foreach ($devices as $device) {

                $cells = $device->cells()->with(['rents', 'device'])
                    ->where('status', '=', Cell::IN_USE)
                    ->whereHas('rents', function ($q) use($user) {
                        $q->where('user_id', '=', $user->id);
                        $q->whereNull('cancelled_at');
                        $q->where('end_date', '>', Carbon::now());
                    })->get();

                foreach ($cells as &$cell) {
                    $place = $cell->device->place;
                    $cell->address = $place->address;
                    $cell->latitude = $place->latitude;
                    $cell->longitude = $place->longitude;
                    $cell->current_temp = round($cell->device->current_temp);
                    $cell->target_temp = $cell->device->target_temp;

                    $possibleRent = $cell->current_rent;
                    $rent = $cell->status == Cell::IN_USE && $possibleRent ? $possibleRent : null;

                    if($rent) {
                        $cell->rent = [
                            'rent_start' => $rent->start_date->format('Y-m-d H:i'),
                            'rent_ends' => $rent->end_date->diffInHours(Carbon::now()) . ':' . $rent->end_date->diff(Carbon::now())->format('%I'),
                            'rent_ends_datetime' => $rent->end_date->format('Y-m-d H:i'),
                            'is_active' => (bool)$rent->is_active
                        ];
                    }

                    $cell->mac = $device->mac;
                    $cell->zone = $cell->device->getLabel();
                    $cell->is_door_open = (bool)$cell->is_door_open;

                    $cell->setHidden(['created_at', 'updated_at', 'opens_total', 'opens_by_rent', 'rents', 'rent_id', 'status', 'device', 'current_rent']);
                    $result[] = $cell;
                }

        }

        return $result;
    }

    public function rentCell($user, $temp, Carbon $sinceDate, $hours, $sizeIds = [], $placeId = 0)
    {
        $sinceClone = clone($sinceDate);
        $due = is_null($hours)
            ? $sinceClone->addHours(48)
            : $sinceClone->addHours($hours);


        $devices = $user->getDevicesWithCellsBuilder($temp);
        $device = $devices->when($placeId > 0, function ($query) use($placeId) {
            $query->where('place_id', $placeId);
        })->when($sizeIds, function ($query) use($sizeIds) {
                $query->whereHas('cells', function ($q) use($sizeIds) {
                    $q->whereIn('size_id', $sizeIds)
                        ->where('status','=', Cell::FREE);
            });
        }, function ($query) {
            $query->whereHas('cells', function ($q) {
                $q->where('status','=', Cell::FREE);
            });
        })->first();
        $cell = null;
        if($device) {
            $cell = $device->cells()->where('status','=', Cell::FREE)
                ->when($sizeIds, function ($q) use($sizeIds) {
                    $q->whereIn('size_id', $sizeIds)
                        ->where('status','=', Cell::FREE);
                })
                ->orderBy('opens_total', 'asc')
                ->first();
        }

        if($cell) {
            $rent = Rent::create([
                'user_id' => $user->id,
                'cell_id' => $cell->id,
                'start_date' => $sinceDate,
                'end_date' => $due,
                'is_active' => $sinceDate->lte(Carbon::now())
            ]);

            $cell->rent_id = $rent->id;
            $cell->status = Cell::IN_USE;
            $cell->save();

            return [
                'cell' => [
                    'id' => $cell->id,
                    'number' => $cell->number,
                    'mac' => $device->mac,
                    'dimension' => $cell->size->label
                ],
                'from' => $sinceDate->format('Y-m-d H:i'),
                'due' => $due->format('Y-m-d H:i')
            ];
        }

        return null;
    }
}
