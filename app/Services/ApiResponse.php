<?php

namespace App\Services;

class ApiResponse
{
    public function success(array $items, $description, $message = 'ok')
    {
        return response()->json([
            'statusCode' => 0,
            'message' => $message,
            'description' => $description,
            'data' => $items
        ]);
    }

    public function failure($description, $code = 400, $message = 'error')
    {
        return response()->json([
            'statusCode' => $code,
            'message' => $message,
            'description' => $description,
            'data' => ["status" => "error"]
        ]);
    }
}
