<?php

namespace App\Imports;

use App\Models\PhoneAddress;
use App\Models\Place;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PhoneAddressImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $addresses = collect(Place::select('id', 'address')->get());
        $existingPhones = PhoneAddress::get()->pluck('phone')->toArray();

        foreach ($rows as $row) {
            $knownAddress = $addresses->filter(function ($item) use($row) {
                return $item->address == $row[1];
            })->first();

            if($knownAddress && !in_array($row[0], $existingPhones)) {
                PhoneAddress::insert([
                    'phone' => $row[0],
                    'place_id' => $knownAddress->id
                ]);
            }
        }
    }
}
