<?php


namespace App\DTO;


use ReflectionClass;
use ReflectionProperty;

class DataTransferObject
{
    public function __construct(array $parameters)
    {
        $class = new \ReflectionClass(static::class);
        $properties = $class->getProperties(\ReflectionProperty::IS_PUBLIC);

        foreach ($properties as $property){
            $propertyName = $property->getName();
            $this->{$propertyName} = $parameters[$propertyName];
        }
    }

    public function toArray()
    {
        $class = new ReflectionClass($this);

        return collect($class->getProperties(ReflectionProperty::IS_PUBLIC))
            ->mapWithKeys(function (ReflectionProperty $property) {
                return [$property->getName() => $this->{$property->getName()}];
            })->toArray();
    }

    public static function fromArray(array $data)
    {
        $class = new ReflectionClass(static::class);

        $result = collect($class->getProperties(ReflectionProperty::IS_PUBLIC))
            ->mapWithKeys(function (ReflectionProperty $property) use ($data) {
                return [$property->getName() => $data[$property->getName()] ?? ''];
            })->toArray();

        return new static($result);
    }
}
