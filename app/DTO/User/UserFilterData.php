<?php

namespace App\DTO\User;

use App\DTO\DataTransferObject;
use Illuminate\Http\Request;

class UserFilterData extends DataTransferObject
{
    public $firstName;
    public $lastName;
    public $phone;


    public static function fromRequest(Request $request)
    {
        return new self([
            'firstName' => $request->get('first_name'),
            'lastName' => $request->get('last_name'),
            'phone' => $request->get('phone'),
        ]);
    }
}
