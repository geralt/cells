<?php

namespace App\Console\Commands;

use App\Models\Device;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class FillTokenForOldDevices extends Command
{
    protected $signature = 'fill_token_for_devices';
    protected $description = 'Fill token field for previously created devices';

    public function handle()
    {
        $devices = Device::whereNull('token')->get();
        foreach ($devices as $device) {
            $device->token = Str::random(40);
            $device->save();
        }
    }
}
