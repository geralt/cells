<?php

namespace App\Console\Commands;

use App\Models\Device;
use App\Models\DeviceTemperature;
use App\Services\DeviceService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TemperatureSensorsPolling extends Command
{
    const BLOCK_SIZE = 1000;

    protected $signature = 'sensor_polling';
    protected $description = 'Poll temperature sensors';

    private $deviceService;

    public function __construct(DeviceService $deviceService)
    {
        parent::__construct();
        $this->deviceService = $deviceService;
    }

    public function handle()
    {
        foreach ($this->getDevices() as $device) {
            try {
                $answer = json_decode($this->deviceService->request($device->ip_address . '/sensors'), true);
                if($answer) {
                    $temps = [];
                    foreach ($answer[0]['sensor'] as $sensorId => $temperature) {
                        $temps[] = $temperature;
                    }
                    if(count($temps) > 0) {
                        $now = Carbon::now();
                        $maxTemp = max($temps);
                        $device->current_temp = $maxTemp;
                        $device->answered_at = $now;
                        $device->save();

                        DeviceTemperature::create([
                            'device_id' => $device->id,
                            'temp' => $maxTemp,
                            'created_at' => $now
                        ]);
                    }
                }
            } catch(\Exception $e) {
                info("device id: " . $device->id . ' ' .$e->getMessage() . "\n" . $e->getTraceAsString());
            }
        }
    }

    private function getDevices()
    {
        $devices = Device::get();
        foreach ($devices as $device) {
            yield $device;
        }
    }
}
