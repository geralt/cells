<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\UserPlace;
use Illuminate\Console\Command;

class MigrateUserPlacesToNewTable extends Command
{
    protected $signature = 'user_places_to_own_table';
    protected $description = 'Moved user places to its own table';

    public function handle()
    {
        $users = User::all();
        foreach ($users as $user) {
            if($user->place_id) {
                UserPlace::create([
                    'user_id' => $user->id,
                    'place_id' => $user->place_id
                ]);
            }
        }
    }
}
