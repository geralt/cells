<?php

namespace App\Console\Commands;

use App\Models\Rent;

use App\Services\FCMService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RentStatusCommand extends Command
{
    const BLOCK_SIZE = 1000;

    private $fcmService;

    protected $signature = 'update_rent_status';
    protected $description = 'Change rent and cell status';

    public function __construct(FCMService $fcmService)
    {
        parent::__construct();
        $this->fcmService = $fcmService;
    }

    public function handle()
    {
        $now = Carbon::now();
        $group = [];
        $counter = 0;
        $rents = Rent::with(['cell', 'user'])
            ->where('created_at', '>=', Carbon::now()->subMonth())
            ->get();
        foreach ($rents as $rent) {
            if($rent->is_active == 1 && ($rent->cancelled_at || $rent->end_date->lt($now) )) {
                $rent->is_active = 0;

                $this->sendPush($rent, [
                    'title' => 'Аренда завершена',
                    'body' => 'Адрес: ' . $rent->cell->device->place->address . ', ячейка №' . $rent->cell->number
                ]);

                if($rent->is_active == 0 && $rent->cell->status == 1) {
                    $rent->cell->status = 0;
                    $rent->cell->save();
                }
            }

            if($rent->is_active == 0 && $rent->start_date->lte($now) && $rent->end_date->gt($now) && !$rent->cancelled_at) {
                $rent->is_active = 1;

                $this->sendPush($rent, [
                    'title' => 'Аренда началась',
                    'body' => 'Адрес: ' . $rent->cell->device->place->address . ', ячейка №' . $rent->cell->number
                ]);

                if($rent->is_active == 1 && $rent->cell->status == 0) {
                    $rent->cell->status = 1;
                    $rent->cell->save();
                }
            }

            $rent->save();

            if($rent->is_active == 1 && $rent->end_date->diffInMinutes(Carbon::now()) === 60) {
                $this->sendPush($rent, [
                    'title' => 'Час до окончания аренды',
                    'body' => 'Адрес: ' . $rent->cell->device->place->address . ', ячейка №' . $rent->cell->number
                ]);
            }

            /*$group[] = $rent;
            $counter++;

            if($counter > self::BLOCK_SIZE) {
                $this->save($group);
            }*/
        }
        //$this->save($group);
    }

    private function getRents()
    {
        $rents = Rent::with(['cell', 'user'])
            ->where('created_at', '>=', Carbon::now()->subMonth())
            ->get();
        foreach ($rents as $rent) {
            yield $rent;
        }
    }

    /*private function save($group)
    {
        DB::beginTransaction();
        // do all your updates here

        foreach ($group as $item) {

            if($item->is_active == 0 && $item->cell->status == 1) {
                $item->cell->status = 0;
                $item->cell->save();
            }

            if($item->is_active == 1 && $item->cell->status == 0) {
                $item->cell->status = 1;
                $item->cell->save();
            }
        }
        // when done commit
        DB::commit();
    }*/

    private function sendPush(Rent $rent, array $data)
    {
        if($rent->user->device_id) {
            $this->fcmService->send($rent->user->device_id, $data['title'], $data['body']);
        }
    }
}
