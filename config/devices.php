<?php

return [
    'cells_count' => [10, 20, 30],
    'possible_temp' => ['-18', '+4', '+20', '+40']
];
