<?php

return [
    'account_sid' => env('TWILIO_ACCOUNT_ID'),
    'auth_token' => env('TWILIO_AUTH_TOKEN'),
    'service_id' => env('TWILIO_SERVICE_ID')
];
